<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/jqvmap/dist/jqvmap.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/weathericons/css/weather-icons.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/weathericons/css/weather-icons-wind.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/summernote/dist/summernote-bs4.css">
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
        </div>
        <p class="section-lead">
            <?= $this->session->flashdata('message') ?>
        </p>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Produk</h4>
                        </div>
                        <div class="card-body">
                            <?= $produk ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pelanggan</h4>
                        </div>
                        <div class="card-body">
                            <?= $user ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-file"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pesanan</h4>
                        </div>
                        <div class="card-body">
                            <?= $pesanan ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-money-bill-wave"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pendapatan</h4>
                        </div>
                        <div class="card-body">
                            <?= rupiah($pendapatan) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Grafik Penjualan Perbulan</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" height="100"></canvas>
                        <div class="statistic-details mt-sm-4">
                            <div class="statistic-details-item">
                                <div class="detail-value"><?= rupiah($hariIni); ?></div>
                                <div class="detail-name">Penjualan Hari ini</div>
                            </div>
                            <div class="statistic-details-item">
                                <div class="detail-value"><?= rupiah($bulanIni); ?></div>
                                <div class="detail-name">Penjualan Bulan ini</div>
                            </div>
                            <div class="statistic-details-item">
                                <div class="detail-value"><?= rupiah($tahunIni); ?></div>
                                <div class="detail-name">Penjualan Tahun ini</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Pelanggan Baru</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled list-unstyled-border">
                            <?php foreach ($pelanggan as $row) : ?>
                                <li class="media">
                                    <img class="mr-3 rounded-circle" width="50" src="assets/img/avatar/avatar-1.png" alt="avatar">
                                    <div class="media-body">
                                        <div class="media-title"><?= $row['nama']; ?></div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Order Baru</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled list-unstyled-border">
                            <?php foreach ($transaksi as $row) : ?>
                                <li class="media">
                                    <div class="media-body">
                                        <div class="media-title">ORDER <a href="<?= base_url('transaksi/detailPesanan/' . $row['id_transaksi']) ?>" style="color:blue; text-decoration: underline;"><?= $row['kode_transaksi']; ?></a></div>
                                        <div class="text-muted text-small">by <a href="#"><?= $row['nama']; ?></a>
                                            <div class="bullet"></div> <?= rupiah($row['total_pembayaran']); ?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                        <div class="text-center pt-1 pb-1">
                            <a href="<?= base_url('transaksi/index') ?>" class="btn btn-primary btn-lg btn-round">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="d-inline">Pembayaran Menunggu Konfirmasi</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled list-unstyled-border">
                            <?php foreach ($pembayaran as $row) : ?>
                                <li class="media">
                                    <div class="media-body">
                                        <h6 class="media-title"><a href="<?= base_url('pembayaran/detailPembayaran/' . $row['id_transaksi'] . '/' . $row['id_pembayaran']) ?>" style="color:blue; text-decoration: underline;"><?= $row['kode_transaksi']; ?></a></h6>
                                        <div class="text-small text-muted">by <a href="#"><?= $row['nama']; ?> <div class="bullet"></div> <?= rupiah($row['total_pembayaran']); ?></div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                        </ul>
                        <div class="text-center pt-1 pb-1">
                            <a href="<?= base_url('pembayaran/verifikasiPembayaran') ?>" class="btn btn-primary btn-lg btn-round">
                                View All
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-7 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Produk Baru</h4>
                        <div class="card-header-action">
                            <a href="<?= base_url('produk') ?>" class="btn btn-primary">View All</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Stok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($produktiga as $row) : ?>
                                        <tr>
                                            <td>
                                                <?= $no; ?>
                                            </td>
                                            <td>
                                                <?= $row['nama_produk']; ?>
                                            </td>
                                            <td>
                                                <?= rupiah($row['harga_produk']); ?>
                                            </td>
                                            <td>
                                                <?= $row['jumlah_stok']; ?>
                                            </td>
                                        </tr>
                                    <?php
                                        $no++;
                                    endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Kategori Produk</h4>
                        <div class="card-header-action">
                            <a href="<?= base_url('kategoriProduk') ?>" class="btn btn-primary">View All</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($dataKategori as $row) :
                                    ?>
                                        <tr>
                                            <td>
                                                <?= $no; ?>
                                            </td>
                                            <td>
                                                <?= $row['nama_kategori']; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?= base_url('assets/') ?>node_modules/simpleweather/jquery.simpleWeather.min.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/chart.js/dist/Chart.min.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/summernote/dist/summernote-bs4.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script>


<!-- Page Specific JS File -->
<script>
    var statistics_chart = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(statistics_chart, {
        type: 'line',
        data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
            datasets: [{
                label: 'Statistics',
                // data: [

                //     1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000,
                //     1000000,
                //     1000000
                // ],
                data: JSON.parse(`<?= json_encode($transaksiMonth) ?>`),
                borderWidth: 5,
                borderColor: '#6777ef',
                backgroundColor: 'transparent',
                pointBackgroundColor: '#fff',
                pointBorderColor: '#6777ef',
                pointRadius: 4
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                    ticks: {
                        stepSize: 200000
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: '#fbfbfb',
                        lineWidth: 2
                    }
                }]
            },
        }
    });
</script>