<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Admin Gudang</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('transaksi/index') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Tabel Data <?= $title ?></h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                Kode Pesanan
                                            </th>
                                            <th>Tanggal Transaksi</th>
                                            <th>Nama Pembeli</th>
                                            <th>Total Pembayaran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($transaksi as $row) :
                                        ?>
                                            <tr>
                                                <td> <a href="<?= base_url('transaksi/detailPesanan/' . $row['id_transaksi']) ?>"><strong><?= $row['kode_transaksi']; ?></strong></a></td>
                                                <td>
                                                    <?= $row['tanggal_transaksi']; ?>
                                                </td>
                                                <td><?= $row['nama']; ?></td>
                                                <td><?= rupiah($row['total_pembayaran']) ?></td>
                                                <td>
                                                    <button onclick="download(<?= $row['id_transaksi'] ?>)" class="btn btn-info"><i class="fas fa-download"></i></button>
                                                </td>
                                            </tr>
                                        <?php
                                            $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function download(id) {
        var url = `<?= base_url('faktur/fakturGenerate/') ?>` + id;
        window.open(url);
    }
</script>