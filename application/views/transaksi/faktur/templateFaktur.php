<html>

<head>
    <title></title>
    <style>
        @page {
            margin: 10px !important;
            padding: 0px !important;
        }

        /* html,body { margin: 1px !important; padding : 0 !important;} */

        .box {
            width: 10px;
            height: 10px;
            border: 1px solid black;
            display: inline-block;
            text-align: center;
        }

        .sizeTable {
            height: auto;
            width: 394px;
            display: inline-block;
        }

        .bungkus {
            width: 791px;
            height: 120px;
        }
    </style>
</head>

<body>

    <center>
        <h4 style="height: 10px;"></h4>
        <table width="100%">
            <tr>
                <td width="10"></td>
                <td colspan="3"></td>
                <td width="50%"></td>
                <td style="text-align: end;">&nbsp; No : 0<?= $transaksi['id_transaksi']; ?> </td>
            </tr>
        </table>
        <!-- ############################################################################## -->
        <center>
            <h4 style="height: 30px; font-size: 30pt;">FACTUR</h4>
        </center>
        <hr style="margin-bottom: 5%; width:95%">
        <!-- <hr> -->

        <table width="100%">
            <tr style="height: 3px;">
                <td width="20"></td>
                <td width="40%"></td>
                <td width="1"></td>
                <td width="80"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    Pengeluaran Garam <br> Menurut
                    Surat Perintah
                    <br> Pengiriman Garam : <br> <br>
                    No. <?= date("Y") ?>/SPP6/IX/<?= $transaksi['id_transaksi']; ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>Kepada
                    <br>
                    <br>
                    Yth. <?= $transaksi['nama']; ?> <br>
                    ........................................ <br>
                    ................. <br>
                </td>
            </tr>

            </tr>

        </table>
        <br>
        <hr>
        <table width="100%">
            <tr style=" height: 3px;">
                <td width="1"></td>
                <td width="20%"></td>
                <td width="1"></td>
                <td width="40%"></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td></td>
                <td align="center">
                    U R A I A N
                </td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">
                    J U M L A H B E R A T </br>
                    Kg
                </td>
            </tr>
        </table>
        <hr>
        <table width="100%" style="border-bottom: 1px solid black;">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                </td>
                <td></td>
                <td></td>
                <td>
                </td>
            </tr>
            <?php
            $no = 1;
            foreach ($keranjang as $row) :
            ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Garam : <?= $row['nama_produk']; ?></td>
                    <td></td>
                    <td></td>
                    <td><?= $row['jumlah_berat'] ?></td>
                </tr>
            <?php
                $no++;
            endforeach; ?>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Diangkut Truck No. : <?= $transaksi['plat_truk'] ?? '-'; ?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">Jumlah : </td>
                <td></td>
                <td></td>
                <td style="border: 1px solid black ;"><?= $transaksi['total_berat']; ?> Kg</td>
            </tr>
        </table>

        <table width="100%">
            <tr style="height: 3px;">
                <td width="0"></td>
                <td width="10%"></td>
                <td width="0"></td>
                <td width="35%"></td>
                <td width="0"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br>
                    Dari
                </td>
                <td>
                    <br>:
                </td>
                <td>
                    <br>
                    ................ , ................. ..............
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br>
                    Pengaraman
                </td>
                <td> <br>:</td>
                <td>
                    <br>Gudang Industri
                </td>
                <td>

                </td>
                <td></td>
                <td>
                    <br>
                    <center>
                        Kasi Gudang Garam Industri <br>
                    </center>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <br>
                    Pabrik
                </td>
                <td><br>
                    :</td>
                <td>
                    <br>................
                </td>
                <td>
                </td>
                <td></td>
                <td>
                    <br>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>

                </td>
                <td></td>
                <td></td>
                <td>
                    <br>
                    <center>
                        ................................................................
                    </center>
                </td>
            </tr>
        </table>

        <table width="100%">
            <tr style="height: 3px;">
                <td width="0"></td>
                <td width="10%"></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td colspan="2">
                    <center>
                        Di terima oleh,<br>
                        Sopir
                        <br>
                        <br>
                        <br>
                        <br>
                        ..........................................................
                    </center>
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </center>
    <!-- ###################################################################################### -->

</body>

</html>