<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data-Master</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('metodePembayaran') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                            </div>
                            <div class="col-md-6 float-right">
                                <button class="btn text-light btn-primary float-right" onclick="addMetode()"> <i class="fa fa-plus"></i>
                                    Tambah </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>Nama Metode</th>
                                            <th>Rekening</th>
                                            <th>A/N</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataMetode as $row) :
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= $no ?>
                                                </td>
                                                <td><?= $row['nama_metode'] ?></td>
                                                <td><?= $row['rekening'] ?></td>
                                                <td><?= $row['an'] ?></td>
                                                <td>
                                                    <?php
                                                    if ($row['status'] == 1) : ?>
                                                        <div class="badge badge-success">Aktif</div>
                                                    <?php
                                                    else : ?>
                                                        <div class="badge badge-danger">Non-Aktif</div>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><button class="btn btn-warning" data-toggle="modal" data-target="#editModal<?= $row['id_metode'] ?>">Edit</button>
                                                    <button onclick="deleteMetode(<?= $row['id_metode'] ?>)" class="btn btn-danger">Hapus</button>
                                                    <?php
                                                    if ($row['status'] == 1) : ?>
                                                        <button onclick="nonAktif(<?= $row['id_metode'] ?>)" class="btn" style="background-color: #574B90; color:white">NonAktif</button>
                                                    <?php
                                                    else : ?>
                                                        <button onclick="aktifkan(<?= $row['id_metode'] ?>)" class="btn btn-success">Aktifkan</button>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="inputMetode">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Metode Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-metode-create">
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Nama Metode :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Rekening :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm rekening" name="rekening" type="text" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">A/N :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm an" name="an" type="text" value="" required>
                            </div>
                        </div>
                </div>
                </form>

                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                </div>
            </div>
        </div>
    </div>

    <?php foreach ($dataMetode as $row) : ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="editModal<?= $row['id_metode'] ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Metode Pembayaran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-edit" action="<?= base_url('metodePembayaran/updateMetode/' . $row['id_metode']) ?>" method="POST">
                            <div class="form-group row">
                                <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Nama Metode :</label>
                                <div class="col-sm-12">
                                    <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="<?= $row['nama_metode'] ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Rekening :</label>
                                <div class="col-sm-12">
                                    <input class="form-control form-control-sm rekening" name="rekening" type="text" value="<?= $row['rekening'] ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">A/N :</label>
                                <div class="col-sm-12">
                                    <input class="form-control form-control-sm an" name="an" type="text" value="<?= $row['an'] ?>" required>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                            &times; Cancel</button>
                        <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<script>
    $.addNewMetode = () => {
        $('#submit').on('click', function() {
            $.ajax({
                type: 'POST',
                url: `<?= base_url('metodePembayaran/store') ?>`,
                data: $('#form-metode-create').serialize(),
                success: function(response) {
                    if (response == "success") {
                        $('#inputMetode').modal('hide');
                        alert('Data metode berhasil disimpan');
                        window.location.reload();
                    } else {
                        alert('Data metode gagal Disimpan :(');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

    function deleteMetode(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('metodePembayaran/delete/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data metode pembayaran berhasil didelete');
                    window.location.reload();
                } else {
                    alert('Data metode pembayaran gagal didelete :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function nonAktif(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('metodePembayaran/nonAktifkan/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data metode pembayaran berhasil di nonAktifkan');
                    window.location.reload();
                } else {
                    alert('Data metode pembayaran gagal di nonAktifkan :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function aktifkan(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('metodePembayaran/aktifkan/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data metode pembayaran berhasil di aktifkan');
                    window.location.reload();
                } else {
                    alert('Data metode pembayaran gagal di aktifkan :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function addMetode() {
        $('#inputMetode').modal('show');
        $.addNewMetode();
    }
</script>