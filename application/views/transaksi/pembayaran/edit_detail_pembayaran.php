<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?> #CPP2436473</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Penjualan</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('pembayaran/index') ?>">Pembayaran</a></div>
                <div class="breadcrumb-item active"><a href="#"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                                <span class="text-danger">Transaksi dibuat tanggal : <?= toDate($asemrowo->tanggal_transaksi) ?></span>
                            </div>
                        </div>
                        <form id="formini">
                            <input type="hidden" name="transaksi_id" value="<?= $asemrowo->id_transaksi; ?>">
                            <input type="hidden" name="pembayaran_id" value="<?= $pembayaran[0]['id_pembayaran']; ?>">
                            <input type="hidden" name="bukti_pembayaran" value="<?= $pembayaran[0]['bukti_pembayaran']; ?>">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Nama Pembeli :</label>
                                            <div class="col-sm-12">
                                                <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="<?= $asemrowo->nama; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Total Pembayaran :</label>
                                            <div class="col-sm-12">
                                                <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value=" <?= rupiah($asemrowo->total_pembayaran) ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Metode Pembayaran :</label>
                                            <div class="col-sm-12">
                                                <!-- <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="BNI (123265439) a/n Senna" readonly> -->
                                                <select class="form-control" name="metode_id" id="metode_id" required>
                                                    <option value="" disabled selected>--Silahkan Pilih Metode Pembayaran</option>
                                                    <?php
                                                    foreach ($metodePembayaran as $row) :
                                                    ?>
                                                        <option value="<?= $row['id_metode']; ?>"><?= $row['nama_metode'] . ' (' . $row['rekening'] . ') a/n ' . $row['an'] ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data Barang yang Dibeli</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>Nama Produk</th>
                                            <th>Jumlah Harga</th>
                                            <th>Jumlah Pembelian</th>
                                            <th>Jumlah Berat (kg)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $sumHarga = 0;
                                        $sumBerat = 0;
                                        foreach ($keranjang as $row) :
                                        ?>
                                            <tr>
                                                <td><?= $no; ?></td>
                                                <td><?= $row['nama_produk']; ?></td>
                                                <td><?= rupiah($row['jumlah_harga']); ?></td>
                                                <td><?= $row['jumlah_beli']; ?></td>
                                                <td><?= $row['jumlah_berat']; ?> kg</td>
                                            </tr>
                                        <?php
                                            $sumHarga += $row['jumlah_harga'];
                                            $sumBerat += $row['jumlah_berat'];
                                            $no++;
                                        endforeach; ?>
                                    </tbody>
                                    <tbody>
                                        <tr style="color: red; font-weight: bold;">
                                            <td></td>
                                            <td>Total Harga :</td>
                                            <td><?= rupiah($sumHarga) ?></td>
                                            <td>Total Berat :</td>
                                            <td><?= $sumBerat ?> Kg</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Bukti Pembayaran</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input class="form-control form-control-sm foto" id="buktiBayar" name="buktiBayar" type="file" required>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- add before </body> -->
<script src="https://unpkg.com/filepond/dist/filepond.js"></script>
<!-- add before </body> -->
<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond/dist/filepond.js"></script>
<script>
    $(document).ready(function() {
        // Register the plugin
        FilePond.registerPlugin(FilePondPluginFileValidateType);
        const inputElement = document.querySelector('input[id="buktiBayar"]');
        console.log(inputElement);
        FilePond.create(
            inputElement, {
                credits: false,
                maxFileSize: "3000000",
                acceptedFileTypes: [
                    'image/jpeg',
                    'image/png',
                    'application/pdf',
                ],
                fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                    resolve(type);
                }),
                server: {
                    url: `<?= base_url('pembayaran') ?>`,
                    type: "POST",
                    process: '/process',
                    revert: '/revert',
                    fetch: null
                }
            }
        );
    })

    $('#submit').on('click', function() {
        $.ajax({
            type: 'POST',
            url: `<?= base_url('pembayaran/update') ?>`,
            data: $('#formini').serialize(),
            success: function(response) {
                if (response == "success") {
                    alert('Data pembayaran berhasil disimpan');
                    console.log(response);
                    window.location.href = `<?= base_url('pembayaran/pembayaran'); ?>`;
                } else {
                    alert('Data pembayaran gagal Disimpan :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    })
</script>