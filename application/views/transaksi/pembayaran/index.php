<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Penjualan</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('pembayaran/index') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Tabel Data <?= $title; ?></h4>
                            </div>
                            <div class="col-md-6 float-right">
                                <?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
                                    <?php if (count($transaksiSelesai) != 0) { ?>
                                        <button class="btn text-light btn-primary float-right" onclick="addPembayaran()"> <i class="fa fa-plus"></i>
                                            Tambah </button>
                                    <?php } ?>
                                <?php  } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                Kode Pesanan
                                            </th>
                                            <th>Tanggal Transaksi</th>
                                            <th>Nama Pembeli</th>
                                            <th>Total Harga</th>
                                            <th>Detail</th>
                                            <th>Status</th>
                                            <?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
                                                <th>Aksi</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($dataPembayaran as $row) :
                                        ?>
                                            <tr>
                                                <td> <a href="<?= base_url('transaksi/detailPesanan/' . $row['id_transaksi']) ?>"><strong><?= $row['kode_transaksi']; ?></strong></a></td>
                                                <td>
                                                    <?= $row['tanggal_transaksi']; ?>
                                                </td>
                                                <td><?= $row['nama']; ?></td>
                                                <td><?= rupiah($row['total_pembayaran']); ?></td>
                                                <td><a href="<?= base_url('pembayaran/detailPembayaran/' . $row['id_transaksi'] . '/' . $row['id_pembayaran']) ?>" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Tekan untuk melihat detail"><i class="fas fa-eye"></i></a></td>
                                                <td>
                                                    <?php
                                                    if ($row['status_pembayaran'] == 0) { ?>
                                                        <div class="badge badge-danger">Belum Di Acc</div>
                                                    <?php } else if ($row['status_pembayaran'] == 1) { ?>
                                                        <div class="badge badge-success">Selesai</div>
                                                    <?php }  ?>
                                                </td>
                                                <?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
                                                    <td>
                                                        <a href="<?= base_url('pembayaran/edit/' . $row['id_pembayaran'] . '/' . $row['id_transaksi']); ?>" class="btn btn-warning btn-edit">Edit</a>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                        <?php
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalBayar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pilih Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <form action="<?= base_url('pembayaran/detailPembayaran'); ?>" method="GET">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Transaksi :</label>
                            <div class="col-sm-12">
                                <select name="transaksi_id" id="" class="form-control form-control-sm ">
                                    <?php foreach ($transaksiSelesai as $row) : ?>
                                        <option value="<?= $row['id_transaksi']; ?>"><?= $row['kode_transaksi']; ?></option>
                                    <?php endforeach ?>
                                </select>
                                <!-- <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="" required> -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                            &times; Cancel</button>
                        <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" type="submit">&plus; Monggo dipun bayar aken bu bu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function addPembayaran() {
        $('#modalBayar').modal('show');
    }
</script>