<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Penjualan</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('transaksi/index') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Tabel Data <?= $title ?></h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                Kode Pesanan
                                            </th>
                                            <th>Nama Pembeli</th>
                                            <th>Metode Pembayaran</th>
                                            <th>Total Pembayaran</th>
                                            <th>Bukti Pembayaran</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($pembayaran as $row) : ?>
                                            <tr>
                                                <td><a href="<?= base_url('transaksi/detailPesanan/' . $row['id_transaksi']) ?>"><strong> <?= $row['kode_transaksi']; ?></strong></a></td>
                                                <td><?= $row['nama']; ?></td>
                                                <td><?= $row['nama_metode']; ?></td>
                                                <td><?= rupiah($row['total_pembayaran']); ?></td>
                                                <td><a href="<?= base_url('uploads/' . $row['bukti_pembayaran']) ?>" target="__blank" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Tekan untuk melihat bukti"><i class="fas fa-file"></i></a></td>
                                                <td>
                                                    <?php
                                                    if ($row['status_pembayaran'] == 0) : ?>
                                                        <button onclick="aktifkan(<?= $row['id_pembayaran'] ?>,<?= $row['id_transaksi'] ?>)" class="btn btn-success">Aktifkan</button>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="inputAktifkan">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Aktifkan Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-aktifkan">
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Plat Truk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm plat_truk" name="plat_truk" type="text" value="" required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function aktifkan(idPembayaran, idTransaksi) {
        $('#inputAktifkan').modal('show');
        $.addNewVerifikasi(idPembayaran, idTransaksi);
    }

    $.addNewVerifikasi = (idPembayaran, idTransaksi) => {
        $('#submit').on('click', function() {
            $.ajax({
                type: 'post',
                url: `<?= base_url('pembayaran/aktifkan/') ?>` + idPembayaran + '/' + idTransaksi,
                data: $('#form-aktifkan').serialize(),
                success: function(response) {
                    if (response == "success") {
                        alert('Data pembayaran berhasil di aktifkan');
                        window.location.reload();
                    } else {
                        alert('Data pembayaran gagal di aktifkan :(');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }
</script>