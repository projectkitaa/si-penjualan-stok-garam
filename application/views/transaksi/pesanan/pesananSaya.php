<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?= base_url('pelanggan/keranjang') ?>">Keranjang</a></div>
                <div class="breadcrumb-item"><?= $title ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="invoice">
                <div class="invoice-print">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="invoice-title">
                                <h2><?= $title ?></h2>
                                <div class="invoice-number">Order <?= $transaksi[0]['kode_transaksi']; ?></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6" style="max-width: 50%;">
                                    <address>
                                        <strong>Diberikan Kepada</strong><br>
                                        <?= $_SESSION['datauser']['nama'] ?><br>
                                        <?= $_SESSION['datauser']['notelp'] ?><br>
                                        <?= $_SESSION['datauser']['email'] ?><br>
                                        <?= $_SESSION['datauser']['alamat'] ?><br>
                                    </address>
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <address>
                                        <strong>Dikirimkan Oleh:</strong><br>
                                        PT. XYZ Indonesia<br>
                                    </address>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-6 text-md-right">
                                    <address>
                                        <strong>Tanggal Transaksi:</strong><br>
                                        <?= dateTime() ?><br><br>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="section-title">Ringkasan Order</div>
                            <p class="section-lead">Semua produk dalam list ini tidak dapat di delete.</p>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-md">
                                    <tr>
                                        <th data-width="40">#</th>
                                        <th>Produk</th>
                                        <th>Harga Satuan</th>
                                        <th>Jumlah Beli</th>
                                        <th>jumlah Berat</th>
                                        <th>Jumlah Harga</th>
                                    </tr>
                                    <?php
                                    $no = 1;
                                    $sum = 0;
                                    foreach ($keranjang as $row) :
                                    ?>
                                        <tr>
                                            <td><?= $no; ?></td>
                                            <td><?= $row['nama_produk']; ?></td>
                                            <td><?= rupiah($row['harga_produk']); ?></td>
                                            <td><?= $row['jumlah_beli']; ?></td>
                                            <td><?= $row['jumlah_berat']; ?> kg</td>
                                            <td><?= rupiah($row['jumlah_harga']) ?></td>
                                        </tr>
                                    <?php
                                        $sum += $row['jumlah_harga'];
                                        $no++;
                                    endforeach; ?>
                                </table>
                            </div>
                            <div class="row mt-4">
                                <div class="col-lg-8">
                                </div>
                                <div class="col-lg-4 text-right">
                                    <div class="invoice-detail-item">
                                        <div class="invoice-detail-name">Total Pembayaran</div>
                                        <div class="invoice-detail-value invoice-detail-value-lg"><?= rupiah($sum) ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="text-md-right">
                    <div class="float-lg-left mb-lg-0 mb-3">
                        <a href="<?= base_url('transaksi/storePesanan/' . $transaksi[0]['id_transaksi']) ?>" class="btn btn-primary btn-icon icon-left"><i class="fas fa-credit-card"></i> Process Payment</a>
                        <a href="<?= base_url('landingPage') ?>" class="btn btn-danger btn-icon icon-left"><i class="fas fa-times"></i> Cancel</a>
                    </div>
                    <button class="btn btn-warning btn-icon icon-left" disabled></button>
                </div>
            </div>
        </div>
    </section>
</div>