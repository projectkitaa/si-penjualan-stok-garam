<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?> <?= $transaksi[0]['kode_transaksi'] ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Penjualan</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('transaksi/index') ?>">Pesanan</a></div>
                <div class="breadcrumb-item active"><a href="#"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                                <span class="text-danger">Transaksi dibuat tanggal : <?= $transaksi[0]['tanggal_transaksi'] ?></span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Nama Pembeli :</label>
                                        <div class="col-sm-12">
                                            <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="<?= $transaksi[0]['nama'] ?? '-' ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Total Pembayaran :</label>
                                        <div class="col-sm-12">
                                            <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="<?= rupiah($transaksi[0]['total_pembayaran']) ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Plat Truk :</label>
                                        <div class="col-sm-12">
                                            <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="<?= $transaksi[0]['plat_truk'] ?? '-' ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Metode Pembayaran :</label>
                                        <div class="col-sm-12">
                                            <input class="form-control form-control-sm nama_metode" name="nama_metode" type="text" value="<?= $transaksi[0]['nama_metode'] ?? '-' ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data Status Pesanan</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <?php
                                if ($transaksi[0]['status_transaksi'] == 0) { ?>
                                    <div class="badge badge-danger">
                                        <h5 class="mt-1 ml-1 mr-1 mb-1"> Belum Dibayar </h5>
                                    </div>
                                <?php } else if ($transaksi[0]['status_transaksi'] == 1) { ?>
                                    <div class="badge badge-info">
                                        <h5 class="mt-1 ml-1 mr-1 mb-1"> Proses </h5>
                                    </div>
                                <?php } else if ($transaksi[0]['status_transaksi'] == 2) { ?>
                                    <div class="badge badge-warning">
                                        <h5 class="mt-1 ml-1 mr-1 mb-1"> Pengiriman </h5>
                                    </div>
                                <?php } else if ($transaksi[0]['status_transaksi'] == 3) { ?>
                                    <div class="badge badge-success">
                                        <h5 class="mt-1 ml-1 mr-1 mb-1"> Pesanan Telah Selesai </h5>
                                    </div>
                                <?php }  ?>

                            </div>
                            <?php if ($_SESSION['datauser']['role_id'] == 2) { ?>
                                <form action="" method="post" id="form-status">
                                    <div class="form-group">
                                        <label for="status" class="  col-form-label col-form-label-sm">Ubah Status Pesanan :</label>
                                        <select class="form-control" name="status" id="status" required>
                                            <option value="" disabled selected>--Silahkan Pilih Jika Ingin Mengubah Status Pesanan</option>
                                            <option value="1" <?php if ($transaksi[0]['status_transaksi'] == 1) : ?> selected <?php endif ?>>Proses</option>
                                            <option value="2" <?php if ($transaksi[0]['status_transaksi'] == 2) : ?> selected <?php endif ?>>Pengiriman</option>
                                            <option value="3" <?php if ($transaksi[0]['status_transaksi'] == 3) : ?> selected <?php endif ?>>Selesai</option>
                                        </select>
                                    </div>
                                    <button class="btn btn-primary" style="background-color: #1f5e86;border-radius: 30px;" onclick="editStatus(<?= $transaksi[0]['id_transaksi'] ?>)"><i class="fas fa-edit"></i> Ubah</button>
                                </form>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data Barang yang Dibeli</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>Nama Produk</th>
                                            <th>Jumlah Harga</th>
                                            <th>Jumlah Pembelian</th>
                                            <th>Jumlah Berat (kg)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $sumHarga = 0;
                                        $sumBerat = 0;
                                        foreach ($keranjang as $row) :
                                        ?>
                                            <tr>
                                                <td><?= $no; ?></td>
                                                <td><?= $row['nama_produk']; ?></td>
                                                <td><?= rupiah($row['jumlah_harga']); ?></td>
                                                <td><?= $row['jumlah_beli']; ?></td>
                                                <td><?= $row['jumlah_berat']; ?> kg</td>
                                            </tr>
                                        <?php
                                            $sumHarga += $row['jumlah_harga'];
                                            $sumBerat += $row['jumlah_berat'];
                                            $no++;
                                        endforeach; ?>
                                    </tbody>
                                    <tbody>
                                        <tr style="color: red; font-weight: bold;">
                                            <td></td>
                                            <td>Total Harga :</td>
                                            <td><?= rupiah($sumHarga) ?></td>
                                            <td>Total Berat :</td>
                                            <td><?= $sumBerat ?> Kg</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function editStatus(id) {
        $.ajax({
            type: 'post',
            url: `<?= base_url('transaksi/updateStatus/') ?>` + id,
            data: $('#form-status').serialize(),
            success: function(response) {
                if (response == "success") {
                    alert('Berhasil update status');
                    window.location.reload();
                } else {
                    alert('Gagal update status :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
</script>