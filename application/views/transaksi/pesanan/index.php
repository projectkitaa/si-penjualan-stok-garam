<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data Penjualan</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('transaksi/index') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Tabel Data <?= $title ?></h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                Kode Pesanan
                                            </th>
                                            <th>Tanggal Transaksi</th>
                                            <th>Nama Pembeli</th>
                                            <th>Total Harga</th>
                                            <th>Detail</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($pesanan as $row) :
                                        ?>
                                            <tr>
                                                <td><strong><?= $row['kode_transaksi']; ?></strong></td>
                                                <td>
                                                    <?= $row['tanggal_transaksi']; ?>
                                                </td>
                                                <td><?= $row['nama']; ?></td>
                                                <td><?= $row['total_pembayaran']; ?></td>
                                                <td><a href="<?= base_url('transaksi/detailPesanan/' . $row['id_transaksi']) ?> " class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Tekan untuk melihat detail"><i class="fas fa-eye"></i></a></td>
                                                <td>
                                                    <?php
                                                    if ($row['status_transaksi'] == 0) { ?>
                                                        <div class="badge badge-danger">Belum Dibayar</div>
                                                    <?php } else if ($row['status_transaksi'] == 1) { ?>
                                                        <div class="badge badge-info">Proses</div>
                                                    <?php } else if ($row['status_transaksi'] == 2) { ?>
                                                        <div class="badge badge-warning">Pengiriman</div>
                                                    <?php } else if ($row['status_transaksi'] == 3) { ?>
                                                        <div class="badge badge-success">Selesai</div>
                                                    <?php }  ?>
                                                </td>
                                            </tr>
                                        <?php
                                            $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>