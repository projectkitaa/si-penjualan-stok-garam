<style>
    .checked {
        color: orange;
    }
</style>

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Pelanggan</a></div>
                <div class="breadcrumb-item"><a href="#"><?= $title; ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title; ?></h4>
                            </div>
                            <div class="col-md-6 float-right">
                                <?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
                                    <?php if (count($transaksiSelesai) != 0) { ?>
                                        <button class="btn text-light btn-primary float-right" onclick="addReview()"> <i class="fa fa-plus"></i>
                                            Tambah </button>
                                    <?php } ?>
                                <?php  } ?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th style="max-width: 50px;">
                                                No
                                            </th>
                                            <?php if ($_SESSION['datauser']['role_id'] == 2) { ?>
                                                <th>Nama Pembeli</th>
                                            <?php } ?>
                                            <th>Transaksi</th>
                                            <th>Skor</th>
                                            <th width="50%">Komentar</th>
                                            <?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
                                                <th>Aksi</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataReview as $row) :
                                        ?>
                                            <tr>
                                                <td><?= $no; ?></td>
                                                <?php if ($_SESSION['datauser']['role_id'] == 2) { ?>
                                                    <td><?= $row['nama']; ?></td>
                                                <?php } ?>
                                                <td>
                                                    <a href="<?= base_url('transaksi/detailPesanan') ?>"><?= $row['kode_transaksi']; ?></a>
                                                </td>
                                                <td>
                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                        <span class="fa fa-star 
                                                            <?php if ($i  <= $row['skor']) { ?>
                                                                checked
                                                            <?php } ?>">
                                                        </span>
                                                    <?php } ?>
                                                </td>
                                                <td><?= $row['komentar']; ?></td>
                                                <?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
                                                    <td>
                                                        <button class="btn btn-warning btn-edit" onclick="editReview(<?= $row['id_review'] ?>)">Edit</button>
                                                    </td>
                                                <?php } ?>
                                            </tr>
                                        <?php
                                            $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="inputReview">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-review-create">
                        <div class="form-group">
                            <label for="pertemuan" class="  col-form-label col-form-label-sm">Kode Transaksi :</label>
                            <select class="form-control" name="id_transaksi" id="kode_transaksi" required>
                                <option value="" disabled selected>--Silahkan Pilih Kode Transaksi</option>
                                <?php foreach ($transaksiSelesai as $row) : ?>
                                    <option value="<?= $row['id_transaksi'] ?>">
                                        <?= $row['kode_transaksi'] ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm rating">Skor :</label>
                            <div class="col-sm-12">
                                <input class="rating" id="rating" name="skor" max="5" min="1" oninput="this.style.setProperty('--value', this.value)" step="1" type="range" value="1" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Komentar :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm harga" name="komentar" type="text" value="" required>
                            </div>
                        </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" type="submit">&plus; Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Review Transaksi
                        <p class="edit"></p>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-edit">
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm rating">Skor :</label>
                            <div class="col-sm-12">
                                <input class="rating bintang" id="rating" name="skor" min='1' max="5" oninput="this.style.setProperty('--value', this.value)" style="--value:2;" step="1" type="range" value="1" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Komentar :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm harga" id="komentar" name="komentar" type="text" value="" required>
                            </div>
                        </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" type="submit">&plus; Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $.addNewReview = () => {
        $('#form-review-create').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: `<?= base_url('review/store') ?>`,
                data: $('#form-review-create').serialize(),
                success: function(response) {
                    if (response == "success") {
                        $('#inputReview').modal('hide');
                        alert('Data Review berhasil ditambahkan');
                        window.location.reload();
                    } else {
                        alert(response);
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

    $.editReview = (id) => {
        $('#form-edit').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: `<?= base_url('review/update/') ?>` + id,
                data: $('#form-edit').serialize(),
                success: function(response) {
                    if (response == "success") {
                        $('#editModal').modal('hide');
                        alert('Data Review berhasil diubah');
                        window.location.reload();
                    } else {
                        alert(response);
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

    function addReview() {
        $('#inputReview').modal('show');
        $.addNewReview();
    }

    function editReview(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('review/getByid/') ?>` + id,
            success: function(response) {
                var id;
                $.each(JSON.parse(response), function(index, element) {
                    $(".edit").text(element.kode_transaksi);
                    $(".bintang").attr('style', '--value:' + element.skor + ';');
                    $('#komentar').val(element.komentar);
                    // $('#transaksiId').val(element.transaksi_id);
                    id = element.id_review;
                });
                $('#editModal').modal('show');
                $.editReview(id);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
</script>