<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/jqvmap/dist/jqvmap.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/weathericons/css/weather-icons.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/weathericons/css/weather-icons-wind.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>node_modules/summernote/dist/summernote-bs4.css">
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
        </div>
        <p class="section-lead">
            <?= $this->session->flashdata('message') ?>
        </p>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Produk</h4>
                        </div>
                        <div class="card-body">
                            <?= $produk; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pesanan</h4>
                        </div>
                        <div class="card-body">
                            <?= $pesanan; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Stok Produk</h4>
                        </div>
                        <div class="card-body">
                            <?= $stokProduk; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pelanggan</h4>
                        </div>
                        <div class="card-body">
                            <?= $pelanggan; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?= base_url('assets/') ?>node_modules/simpleweather/jquery.simpleWeather.min.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/chart.js/dist/Chart.min.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/summernote/dist/summernote-bs4.js"></script>
<script src="<?= base_url('assets/') ?>node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script>


<!-- Page Specific JS File -->
<script src="<?= base_url('assets/') ?>js/page/index-0.js"></script>