<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data-Master</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('adminGudang') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                            </div>
                            <div class="col-md-6 float-right">
                                <button class="btn text-light btn-primary float-right" onclick="addAdminGudang()"> <i class="fa fa-plus"></i>
                                    Tambah </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>Nama</th>
                                            <th>No Telp</th>
                                            <th>Detail</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataAdminGudang as $row) :
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= $no ?>
                                                </td>
                                                <td><?= $row['nama'] ?></td>
                                                <td><?= $row['notelp'] ?></td>
                                                <td><button class="btn btn-info" onclick="detailAdminGudang(<?= $row['id_user'] ?>)"><i class="fas fa-eye"></i></button></td>
                                                <td>
                                                    <?php
                                                    if ($row['status'] == 1) : ?>
                                                        <div class="badge badge-success">Aktif</div>
                                                    <?php
                                                    else : ?>
                                                        <div class="badge badge-danger">Non-Aktif</div>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><button class="btn btn-warning" onclick="editAdminGudang(<?= $row['id_user'] ?>)">Edit</button>
                                                    <?php
                                                    if ($row['status'] == 1) : ?>
                                                        <button onclick="nonAktif(<?= $row['id_user'] ?>)" class="btn" style="background-color: #574B90; color:white">NonAktif</button>
                                                    <?php
                                                    else : ?>
                                                        <button onclick="aktifkan(<?= $row['id_user'] ?>)" class="btn btn-success">Aktifkan</button>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="inputAdminGudang">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Admin Gudang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-admin-gudang-create">
                        <div class="form-group">
                            <label for="first_name">Nama</label>
                            <input type="text" class="form-control" name="nama" autofocus required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="notelp">Notelp</label>
                            <input type="text" class="form-control" name="notelp" required>
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <div>
                                <textarea name="alamat" class="form-control" required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="d-block">Password</label>
                            <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" required>
                            <div id="pwindicator" class="pwindicator">
                                <div class="bar"></div>
                                <div class="label"></div>
                            </div>
                        </div>
                </div>
                </form>

                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="detail">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Admin Gudang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="first_name">Nama</label>
                        <input id="first_name" type="text" class="form-control nama" name="nama" autofocus readonly>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control email" name="email" readonly>
                    </div>
                    <div class="form-group">
                        <label for="notelp">Notelp</label>
                        <input id="notelp" type="text" class="form-control notelp" name="notelp" readonly>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <div>
                            <textarea name="alamat" id="alamat" class="form-control alamat" readonly></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Admin Gudang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-edit" action="" method="POST">
                        <div class="form-group">
                            <label for="first_name">Nama</label>
                            <input type="text" class="form-control" id="namaEdit" name="nama" value="" autofocus required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="emailEdit" value="" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="notelp">Notelp</label>
                            <input type="text" class="form-control" id="notelpEdit" value="" name="notelp" required>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <div>
                                <textarea name="alamat" id="alamatEdit" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="d-block">Password</label>
                            <input id="passwordEdit" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password">
                            <div id="pwindicator" class="pwindicator">
                                <div class="bar"></div>
                                <div class="label"></div>
                            </div>
                            <label class=" text-danger">*Kosongkan Apabila Tidak Ingin Mengubah Password</label>
                        </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;">&plus; Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $.addNewAdminGudang = () => {
        $('#submit').on('click', function() {
            $.ajax({
                type: 'POST',
                url: `<?= base_url('adminGudang/store') ?>`,
                data: $('#form-admin-gudang-create').serialize(),
                success: function(response) {
                    if (response == "success") {
                        $('#inputAdminGudang').modal('hide');
                        alert('Data Admin Gudang berhasil disimpan');
                        window.location.reload();
                    } else {
                        alert('Data Admin Gudang gagal Disimpan :(');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

    function deleteAdminGudang(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('adminGudang/delete/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data Admin Gudang berhasil didelete');
                    window.location.reload();
                } else {
                    alert('Data Admin Gudang gagal didelete :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function detailAdminGudang(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('adminGudang/getByid/') ?>` + id,
            success: function(response) {
                $.each(JSON.parse(response), function(index, element) {
                    $('.nama').val(element.nama);
                    $('.notelp').val(element.notelp);
                    $('.email').val(element.email);
                    $('.alamat').val(element.alamat);
                });
                $('#detail').modal('show');
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function editAdminGudang(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('adminGudang/getByid/') ?>` + id,
            success: function(response) {
                $.each(JSON.parse(response), function(index, element) {
                    $('#namaEdit').val(element.nama);
                    $('#notelpEdit').val(element.notelp);
                    $('#emailEdit').val(element.email);
                    $('#alamatEdit').val(element.alamat);
                });
                $("#form-edit").attr("action", `<?= base_url('adminGudang/update/') ?>` + id)
                $('#editModal').modal('show');
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function nonAktif(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('adminGudang/nonAktifkan/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data Admin Gudang berhasil di nonAktifkan');
                    window.location.reload();
                } else {
                    alert('Data Admin Gudang gagal di nonAktifkan :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function aktifkan(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('adminGudang/aktifkan/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data Admin Gudang berhasil di aktifkan');
                    window.location.reload();
                } else {
                    alert('Data Admin Gudang gagal di aktifkan :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function addAdminGudang() {
        $('#inputAdminGudang').modal('show');
        $.addNewAdminGudang();
    }
</script>