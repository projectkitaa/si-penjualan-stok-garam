<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data-Master</a></div>
                <div class="breadcrumb-item"><a href="<?= base_url('kategoriProduk') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                            </div>
                            <div class="col-md-6 float-right">
                                <button class="btn text-light btn-primary float-right" onclick="addKategori()"> <i class="fa fa-plus"></i>
                                    Tambah </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th style="max-width: 50px;">
                                                No
                                            </th>
                                            <th>Nama Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataKategori as $row) :
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= $no ?>
                                                </td>
                                                <td><?= $row['nama_kategori'] ?></td>
                                                <td><button class="btn btn-warning" data-toggle="modal" data-target="#editModal<?= $row['id_kategori'] ?>">Edit</button>
                                                    <button onclick="deleteKategori(<?= $row['id_kategori'] ?>)" class="btn btn-danger">Hapus</button>
                                                </td>
                                            </tr>
                                        <?php $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="inputKategori">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Kategori Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-metode-create">
                        <div class="form-group row">
                            <label for="namakategori" class=" ml-3 col-form-label col-form-label-sm">Nama Kategori :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm nama_kategori" name="nama_kategori" type="text" value="" required>
                            </div>
                        </div>
                </div>
                </form>

                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                </div>
            </div>
        </div>
    </div>

    <?php foreach ($dataKategori as $row) : ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="editModal<?= $row['id_kategori'] ?>">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Kategori Produk</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="form-edit" action="<?= base_url('kategoriProduk/updateKategori/' . $row['id_kategori']) ?>" method="POST">
                            <div class="form-group row">
                                <label for="namaKategori" class=" ml-3 col-form-label col-form-label-sm">Nama Kategori :</label>
                                <div class="col-sm-12">
                                    <input class="form-control form-control-sm nama_kategori" name="nama_kategori" type="text" value="<?= $row['nama_kategori'] ?>" required>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                            &times; Cancel</button>
                        <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<script>
    $.addNewKategori = () => {
        $('#submit').on('click', function() {
            $.ajax({
                type: 'POST',
                url: `<?= base_url('kategoriProduk/store') ?>`,
                data: $('#form-metode-create').serialize(),
                success: function(response) {
                    if (response == "success") {
                        $('#inputKategori').modal('hide');
                        alert('Data Kategori Produk berhasil disimpan');
                        window.location.reload();
                    } else {
                        alert('Data Kategori Produk gagal Disimpan :(');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

    function deleteKategori(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('kategoriProduk/delete/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data Kategori Produk berhasil didelete');
                    window.location.reload();
                } else if (response == "cant") {
                    alert('Data Kategori Produk tidak dapat didelete!!');
                    window.location.reload();
                } else {
                    alert('Data Kategori Produk gagal didelete :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function addKategori() {
        $('#inputKategori').modal('show');
        $.addNewKategori();
    }
</script>