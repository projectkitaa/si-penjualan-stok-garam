<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet" />
<link href="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.css" rel="stylesheet" />
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Admin Gudang</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('produk') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                            </div>
                            <div class="col-md-6 float-right">
                                <button class="btn text-light btn-primary float-right" onclick="addProduk()"> <i class="fa fa-plus"></i>
                                    Tambah </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th style="max-width:100px;">
                                                No
                                            </th>
                                            <th>Nama Kategori</th>
                                            <th>Nama Produk</th>
                                            <th>Gambar Produk</th>
                                            <th>Stok</th>
                                            <th>Harga Produk</th>
                                            <th>Berat Produk</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataProduk as $row) :
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= $no ?>
                                                </td>
                                                <td><?= $row['nama_kategori'] ?></td>
                                                <td><?= $row['nama_produk'] ?></td>
                                                <td><img src="<?= base_url('uploads/') ?><?= $row['gambar_produk']; ?>" alt="Girl in a jacket" width="150" height="150"></td>
                                                <td><?= $row['jumlah_stok'] ?></td>
                                                <td><?= rupiah($row['harga_produk']) ?></td>
                                                <td><?= $row['berat_produk'] ?></td>
                                                <td>
                                                    <button class="btn btn-warning btn-edit" onclick="editProduk(<?= $row['id_produk'] ?>)">Edit</button>
                                                    <button onclick="deleteProduk(<?= $row['id_produk'] ?>)" class="btn btn-danger">Hapus</button>
                                                </td>
                                            </tr>
                                        <?php $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="inputProduk">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="form-produk-create" enctype='multipart/form-data'>
                        <div class="form-group">
                            <label for="pertemuan" class="  col-form-label col-form-label-sm">Kategori Produk :</label>
                            <select class="form-control" name="id_kategori" required>
                                <option value="" disabled selected>--Silahkan Pilih Kategori</option>
                                <?php foreach ($kategori as $row) : ?>
                                    <option value="<?= $row['id_kategori'] ?>">
                                        <?= $row['nama_kategori'] ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Nama Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm" name="nama_produk" type="text" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Foto Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm" id="foto_produk" name="foto_produk" type="file" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Harga Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm" name="harga_produk" type="number" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Berat Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm" name="berat_produk" type="number" value="" required>
                            </div>
                        </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" type="submit">&plus; Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-edit" action="<?= base_url('produk/update'); ?>" method="POST">
                        <input type="hidden" name="gambar_produk" id="gambar_produk">
                        <input type="hidden" name="id_produk" id="id_produk">
                        <div class="form-group">
                            <label for="pertemuan" class="  col-form-label col-form-label-sm">Kategori Produk :</label>
                            <select class="form-control" name="id_kategori" id="kategorid" required>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="nama" class=" ml-3 col-form-label col-form-label-sm">Nama Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm nama" name="nama_produk" type="text" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="foto" class=" ml-3 col-form-label col-form-label-sm">Foto Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm foto" id="foto_produk_edit" name="foto_produk" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="harga" class=" ml-3 col-form-label col-form-label-sm">Harga Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm harga" name="harga_produk" type="number" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="berat" class=" ml-3 col-form-label col-form-label-sm">Berat Produk :</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm berat" name="berat_produk" type="number" value="" required>
                            </div>
                        </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- add before </body> -->
<script src="https://unpkg.com/filepond/dist/filepond.js"></script>
<!-- add before </body> -->
<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-poster/dist/filepond-plugin-file-poster.js"></script>
<script>
    $.addNewProduk = () => {
        $('#form-produk-create').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: `<?= base_url('produk/store') ?>`,
                // data: $('#form-produk-create').serialize(),
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(response) {
                    console.log(response);
                    if (response == "success") {
                        $('#inputProduk').modal('hide');
                        alert('Data produk berhasil ditambahkan');
                        window.location.reload();
                    } else {
                        alert(response);
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

    function deleteProduk(id) {

        $.ajax({
            type: 'get',
            url: `<?= base_url('produk/delete/') ?>` + id,
            success: function(response) {
                if (response == "success") {
                    alert('Data produk berhasil didelete');
                    window.location.reload();
                } else {
                    alert('Data produk gagal didelete :(');
                }
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    function addProduk() {
        $('#inputProduk').modal('show');
        $.addNewProduk();
    }

    function editProduk(id) {
        var gambar;
        var kategori_id;

        $.ajax({
            type: 'get',
            url: `<?= base_url('produk/getByid/') ?>` + id,
            success: function(response) {
                $.each(JSON.parse(response), function(index, element) {
                    $('.nama').val(element.nama_produk);
                    $('.harga').val(element.harga_produk);
                    $('.berat').val(element.berat_produk);
                    $('#gambar_produk').val(element.gambar_produk);
                    $('#id_produk').val(id);
                    gambar = element.gambar_produk;
                    kategori_id = element.kategori_id;
                });

                $('#kategorid')
                    .find('option')
                    .remove();

                // $('#kategorid')
                //     .find('option')
                //     .end()
                //     .append("<option value='default' disabled selected='selected'>--Silahkan Pilih Kategori</option>")
                //     .val("default");

                <?php foreach ($kategori as $jn) : ?>
                    $('#kategorid')
                        .find('option')
                        .end()
                        .append(`<option value="<?= $jn['id_kategori'] ?>"> ` + `<?= $jn['nama_kategori'] ?>` + "</option>")
                        .val(kategori_id);
                <?php endforeach; ?>

                // Register the plugin
                let b = document.querySelector('input[id="foto_produk_edit"]');
                let pond = null
                pond = FilePond
                pond.registerPlugin(
                    FilePondPluginFileValidateType, FilePondPluginImagePreview,
                    FilePondPluginFilePoster);


                pond.create(
                    b, {
                        credits: false,
                        maxFileSize: "3000000",
                        acceptedFileTypes: [
                            'image/jpeg',
                            'image/png',
                        ],
                        fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                            resolve(type);
                        }),
                        server: {
                            url: `<?= base_url('produk') ?>`,
                            type: "POST",
                            process: '/process',
                            revert: '/revert',
                            fetch: null
                        },

                    });


                $("#form-edit").attr("action", `<?= base_url('produk/updateProduk/') ?>` + id)
                $('#editModal').modal('show');
            },
            error: function(error) {
                console.log(error);
            }
        });
    }

    $(document).ready(function() {
        // Register the plugin
        let a = FilePond
        a.registerPlugin(FilePondPluginFileValidateType, FilePondPluginImagePreview,
            FilePondPluginFilePoster);
        const inputElement = document.querySelector('input[id="foto_produk"]');
        // console.log(inputElement);
        a.create(
            inputElement, {
                credits: false,
                maxFileSize: "3000000",
                acceptedFileTypes: [
                    'image/jpeg',
                    'image/png',
                ],
                fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                    resolve(type);
                }),
                server: {
                    url: `<?= base_url('produk') ?>`,
                    type: "POST",
                    process: '/process',
                    revert: '/revert',
                    fetch: null
                },
            }
        );
    })
</script>