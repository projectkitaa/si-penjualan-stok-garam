<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data-Master</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('produk') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th style="max-width:100px;">
                                                No
                                            </th>
                                            <th>Nama Kategori</th>
                                            <th>Nama Produk</th>
                                            <th>Gambar Produk</th>
                                            <th>Stok</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataProduk as $row) :
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= $no ?>
                                                </td>
                                                <td><?= $row['nama_kategori'] ?></td>
                                                <td><?= $row['nama_produk'] ?></td>
                                                <td><?= $row['nama_produk'] ?></td>
                                                <td><?= $row['jumlah_stok'] ?></td>
                                                <td>
                                                    <button class="btn btn-primary " id="btn-tambahStok" data-toggle="modal" data-target="#tambahStok" data-id="<?= $row['id_produk'] ?>" data-stok="<?= $row['jumlah_stok'] ?>">Tambah Stok</button>
                                                </td>
                                            </tr>
                                        <?php $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" tabindex="-1" role="dialog" id="tambahStok">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Stok</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-edit" action="<?= base_url('produk/tambahStok') ?>" method="POST">
                        <input type="hidden" id="modal-id" name="id">
                        <div class="form-group row">
                            <label for="alasan" class=" ml-3 col-form-label col-form-label-sm">Stok</label>
                            <div class="col-sm-12">
                                <input class="form-control form-control-sm" id="modal-stok" name="stok" type="number" required>
                                <span class="text-danger mt-1" style="font-weight: bold;">*Awali Dengan '-' apabila ingin mengurangi stok (cth : -30)</span>
                            </div>
                        </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                    <button class="btn btn-primary" style="background-color: #574B90;border-radius: 30px;" id="submit">&plus; Tambah</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    const btnStok = document.querySelectorAll("#btn-tambahStok");

    for (i = 0; i < btnStok.length; i++) {

        btnStok[i].addEventListener("click", function() {

            let id = $(this).data("id");
            let stok = $(this).data("stok");

            $("#modal-id").val(id);
            $("#modal-stok").val(stok);
        });
    }
</script>