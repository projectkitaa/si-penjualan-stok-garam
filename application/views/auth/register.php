<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                <div class="login-brand">
                    <img src="<?= base_url('assets/') ?>img/logo.png" alt="logo" width="100">
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Register</h4>
                    </div>
                    <?= $this->session->flashdata('message') ?>
                    <div class="card-body">
                        <form action="<?= base_url('auth/store') ?>" method="POST">
                            <div class="form-group">
                                <label for="first_name">Nama</label>
                                <input id="first_name" type="text" class="form-control" name="nama" autofocus required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Notelp</label>
                                <input id="email" type="text" class="form-control" name="notelp" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Alamat</label>
                                <div>
                                    <textarea name="alamat" id="" class="form-control" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="d-block">Password</label>
                                <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" required>
                                <div id="pwindicator" class="pwindicator">
                                    <div class="bar"></div>
                                    <div class="label"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="mt-5 text-muted text-center">
                    Already have an account? <a href="<?= base_url('auth/') ?>">Login</a>
                </div>