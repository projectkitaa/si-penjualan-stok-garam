<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Data-Master</a></div>
                <div class="breadcrumb-item active"><a href="<?= base_url('adminGudang') ?>"><?= $title ?></a></div>
            </div>
        </div>

        <div class="section-body">
            <p class="section-lead">
                <?= $this->session->flashdata('message') ?>
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-md-6">
                                <h4>Data <?= $title ?></h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table-1">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>Nama</th>
                                            <th>No Telp</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($dataPelanggan as $row) :
                                        ?>
                                            <tr>
                                                <td>
                                                    <?= $no ?>
                                                </td>
                                                <td><?= $row['nama'] ?></td>
                                                <td><?= $row['notelp'] ?></td>
                                                <td><button class="btn btn-info" onclick="detailPelanggan(<?= $row['id_user'] ?>)"><i class="fas fa-eye"></i></button></td>
                                            </tr>
                                        <?php $no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog" id="detail">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Pelanggan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-window-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="first_name">Nama</label>
                        <input id="first_name" type="text" class="form-control nama" name="nama" autofocus readonly>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control email" name="email" readonly>
                    </div>
                    <div class="form-group">
                        <label for="notelp">Notelp</label>
                        <input id="notelp" type="text" class="form-control notelp" name="notelp" readonly>
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <div>
                            <textarea name="alamat" id="alamat" class="form-control alamat" readonly></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 30px;">
                        &times; Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function detailPelanggan(id) {
        $.ajax({
            type: 'get',
            url: `<?= base_url('pelanggan/getByid/') ?>` + id,
            success: function(response) {
                $.each(JSON.parse(response), function(index, element) {
                    $('.nama').val(element.nama);
                    $('.notelp').val(element.notelp);
                    $('.email').val(element.email);
                    $('.alamat').val(element.alamat);
                });
                $('#detail').modal('show');
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
</script>