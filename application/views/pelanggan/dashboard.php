<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= $title ?></h1>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Order</h4>
                        </div>
                        <div class="card-body">
                            <?= $order; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-circle fa-spin"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Order Dalam Proses</h4>
                        </div>
                        <div class="card-body">
                            <?= $orderProses; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-money-check"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pembayaran</h4>
                        </div>
                        <div class="card-body">
                            <?= $pembayaran; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fab fa-rocketchat"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Review</h4>
                        </div>
                        <div class="card-body">
                            <?= $review; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>