<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?= base_url('landingPage') ?>">PT. XYZ</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm mt-2 mb-2">
            <a href="<?= base_url('landingPage') ?>"><img src="<?= base_url('assets/') ?>img/logo.png" alt="logo" width="80%"></a>
        </div>
        <ul class="sidebar-menu">
            <?php
            if ($_SESSION['datauser']['role_id'] == 2) { ?>
                <li><a class="nav-link" href="<?= base_url('admin'); ?>"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-header">Data Master</li>
                <li><a class="nav-link" href="<?= base_url('role') ?>"><i class="fas fa-user-tag"></i> <span>Role</span></a></li>
                <li><a class="nav-link" href="<?= base_url('adminGudang/indexx') ?>"><i class="fas fa-user"></i> <span>Pegawai Gudang</span></a></li>
                <li><a class="nav-link" href="<?= base_url('metodePembayaran') ?>"><i class="fas fa-money-check-alt"></i> <span>Metode Pembayaran</span></a></li>
                <li><a class="nav-link" href="<?= base_url('kategoriProduk') ?>"><i class="fas fa-list"></i> <span>Kategori Produk</span></a></li>
                <li><a class="nav-link" href="<?= base_url('produk') ?>"><i class="fas fa-box"></i> <span>Produk</span></a></li>
                <li class="menu-header">Data Penjualan</li>
                <li><a class="nav-link" href="<?= base_url('transaksi/index'); ?>"><i class="fas fa-shopping-cart"></i> <span>Pesanan</span></a></li>
                <li><a class="nav-link" href="<?= base_url('pembayaran/index'); ?>"><i class="fas fa-money-bill-alt"></i> <span> Pembayaran</span></a></li>
                <li><a class="nav-link" href="<?= base_url('pembayaran/verifikasiPembayaran'); ?>"><i class="fas fa-money-bill-wave"></i> <span> Verifikasi Pembayaran</span></a></li>
                <li class="menu-header">Pelanggan</li>
                <li><a class="nav-link" href="<?= base_url('pelanggan/index'); ?>"><i class="fas fa-users"></i> <span>Pelanggan</span></a></li>
                <li><a class="nav-link" href="<?= base_url('review/index'); ?>"><i class="fab fa-rocketchat"></i> <span>Review Pelanggan</span></a></li>
            <?php } else if ($_SESSION['datauser']['role_id'] == 1) { ?>
                <li><a class="nav-link" href="<?= base_url('adminGudang/index'); ?>"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-header">Admin Gudang</li>
                <li><a class="nav-link" href="<?= base_url('produk/kelolaStok') ?>"><i class="fas fa-tasks"></i> <span>Kelola Stok</span></a></li>
                <li><a class="nav-link" href="<?= base_url('faktur'); ?>"><i class="fas fa-file"></i> <span>Faktur</span></a></li>
            <?php } else { ?>
                <li><a class="nav-link" href="<?= base_url('pelanggan/dashboard'); ?>"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
                <li><a class="nav-link" href="<?= base_url('review'); ?>"><i class="fab fa-rocketchat"></i> <span>Review</span></a></li>
                <li class="menu-header">Transaksi</li>
                <li><a class="nav-link" href="<?= base_url('transaksi/index'); ?>"><i class="fas fa-shopping-cart"></i> <span>Pesanan Saya</span></a></li>
                <li><a class="nav-link" href="<?= base_url('pembayaran/pembayaran'); ?>"><i class="fas fa-money-bill"></i> <span>Pembayaran</span></a></li>
            <?php } ?>

    </aside>
</div>