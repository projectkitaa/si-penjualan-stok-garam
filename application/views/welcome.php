<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='shorcut icon' href="<?= base_url('assets/') ?>img/logo.png">
	<title><?= $title ?></title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<style type="text/css">
		.usePadding {
			margin: 0px;
			padding-left: 7%;
			padding-right: 7%;
			padding-top: 2%;
			padding-bottom: 3%;
		}

		.usePadding2 {
			margin: 0px;
			padding-left: 7%;
			padding-right: 7%;
		}

		html {
			scroll-behavior: smooth;
		}

		#myBtn {
			display: none;
			/* Hidden by default */
			position: fixed;
			/* Fixed/sticky position */
			bottom: 20px;
			/* Place the button at the bottom of the page */
			right: 30px;
			/* Place the button 30px from the right */
			z-index: 99;
			/* Make sure it does not overlap */
			border: none;
			/* Remove borders */
			outline: none;
			/* Remove outline */
			background-color: #1f5e86;
			/* Set a background color */
			color: white;
			/* Text color */
			cursor: pointer;
			/* Add a mouse pointer on hover */
			padding: 15px;
			/* Some padding */
			border-radius: 10px;
			/* Rounded corners */
			font-size: 18px;
			/* Increase font size */
		}

		.mySlides {
			display: none;
		}

		#myBtn:hover {
			background-color: #fce600;
		}

		#cartButton:hover {
			background-color: #fce600;
		}
	</style>
</head>

<body>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<div class="content">
		<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-fw fa-arrow-up"></i></button>

		<nav class="navbar col-md-12" id="nav1" style="background-color: #061387; color:white;">
			<div class="nav-menu ml-3">
				<div class="row">
					<span class="nav-item nav-link "><i class="fa fa fa-phone"></i> (081) 5437621</span>
					<a class="nav-item nav-link" href="mailto:sekretariat@ptxyz.com" style="color:white"><i class="fa fa-envelope"></i> sekretariat@ptxyz.com</a>
				</div>
			</div>
			<div class="nav-menu mr-3">
				<div class="row">
					<a style="color:white" class="nav-item nav-link" href="#"><i class="fa fa-facebook-official"></i></i></a>
					<a style="color:white" class="nav-item nav-link" href="https://twitter.com/ptxyzpersero/"><i class="fa fa-twitter-square"></i></a>
					<a style="color:white" class="nav-item nav-link" href="https://www.instagram.com/ptxyzpersero/?hl=id"><i class="fa fa-instagram"></i></a>
				</div>
			</div>
		</nav>
		<hr id="hr1" style="border-bottom: 5px solid #fac800;margin-top: 0px;margin-bottom: 2px;border-top: 0px;">

		<nav class="navbar mb-3 usePadding2" style="background-color: white;">
			<img src="<?= base_url('assets/') ?>img/logo.png" width="4%" class="d-inline-block align-top" alt="">
			<div class="nav-menu">
				<div class="row">
					<a href="<?= base_url('landingPage') ?>" class="nav-link" style="color: black;">
						Home
					</a>
					<a class="nav-item nav-link" href="#produkKami" style="color: black;">
						Produk
					</a>
					<a class="nav-item nav-link" href="#tentangKami" style="color: black;">
						Tentang Kami
					</a>
					<a class="nav-item nav-link" href="#kontakKami" style="color: black;">
						Kontak
					</a>
					<?php if ($cek) { ?>
						<div class="nav-item dropdown">
							<a style="color: black;" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Akun
							</a>
							<div class="dropdown-menu" tabindex="-1" aria-labelledby="navbarDropdown">
								<?php if ($dashboard) : ?>
									<a class="dropdown-item" href="<?= base_url($dashboard) ?>">Dashboard</a>
								<?php endif; ?>
								<a class="dropdown-item" href="<?= base_url('auth/logout') ?>">Logout</a>
							</div>
						</div>
						<?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
							<a class="nav-item nav-link" href="<?= base_url('keranjang') ?>" style="color: white; background-color: #1f5e86;">
								<i class="fa fa-shopping-cart"></i> [<?= $jumlahKeranjang; ?>]
							</a>
						<?php } ?>
					<?php } else { ?>
						<div class="nav-item dropdown">
							<a style="color: black;" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Akun
							</a>
							<div class="dropdown-menu" tabindex="-1" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?= base_url('auth') ?>">Login</a>
								<a class="dropdown-item" href="<?= base_url('auth/register') ?>">Register</a>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</nav>

		<p class="section-lead">
		<h5 style="text-align: center;">
			<?= $this->session->flashdata('message') ?>
		</h5>
		</p>

		<div class="col-md-12">
			<div class="slideshow">
				<img class="mySlides" style="z-index: -1;width: 100%; height: 500px; object-fit: scale-down; margin-bottom: 4%;" src="<?= base_url() ?>assets/img/slide2.jpg" alt="slideshow4">
				<img class="mySlides" style="z-index: -1;width: 100%; height: 500px; object-fit: scale-down; margin-bottom: 4%;" src="<?= base_url() ?>assets/img/bg_3.png" alt="slideshow1">
				<img class="mySlides" style="z-index: -1;width: 100%; height: 500px; object-fit: scale-down; margin-bottom: 4%;" src="<?= base_url() ?>assets/img/slide1.jpg" alt="slideshow2">
				<img class="mySlides" style="z-index: -1;width: 100%; height: 500px; object-fit: scale-down; margin-bottom: 4%;" src="<?= base_url() ?>assets/img/bg.jpg" alt="slideshow3">
				<img class="mySlides" style="z-index: -1;width: 100%; height: 500px; object-fit: scale-down; margin-bottom: 4%;" src="<?= base_url() ?>assets/img/slide3.jpg" alt="slideshow5">
			</div>
		</div>

		<div class="col-md-12 bg-light usePadding" id="tentangKami">
			<center>
				<div class="header">
					<h2>Tentang Kami</h2>
					<hr style="border-bottom: 5px solid #061387;margin-top: 0px;margin-bottom: 20px;border-top: 0px;">
				</div>
			</center>
			<div class="card bg-light" style="border:0px">
				<div class="card-body">
					<p style="text-align: center;">
						PT. XYZ (Persero) Perusahaan BUMN yang bergerak di bidang Produksi Garam tertua di Indonesia sebagai agen pembangunan dan tetap konsisten menjaga terjaminnya ketersediaan Garam Nasional, serta senantiasa berupaya mewujudkan kedaulatan pangan di bidang garam.
					</p>
					<table class="table table-borderless mt-3 ml-4">
						<tr>
							<th></th>
							<th>VISI</th>
							<th></th>
							<th>MISI</th>
						</tr>
						<tr>
							<td style="max-width: 30px; font-size: 30px;"><i class="fa fa-bullhorn" style="color: #17e8d8;"></i></td>
							<td style="max-width: 250px;">Menjadi perusahaan industri garam yang berkualitas dunia</td>
							<td style="max-width: 30px; font-size: 30px;"><i class="fa fa-list" style="color: #17e8d8;"></i></td>
							<td style="max-width: 250px; text-align: justify;">Menjadi produsen garam bahan baku dan derivatnya, serta garam olahan berkualitas dunia untuk memenuhi kebutuhan nasional.<br>
								Berkomitmen menjaga pasokan produk secara berkesinambungan.<br>
								Menjamin kepuasan konsumen dan pemangku kepentingan.</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-12 usePadding" id="produkKami">
			<center>
				<div class="header">
					<h2>Produk Kami</h2>
					<hr style="border-bottom: 5px solid #fac800;margin-top: 0px;margin-bottom: 20px;border-top: 0px;">
				</div>
			</center>
			<div class="row" style="justify-content: center;">

				<?php if (count($produk) != 0) { ?>
					<?php foreach ($produk as $row) : ?>
						<div class="card mr-3 mt-4" style="width: 20%;">
							<img src="<?= base_url('uploads') ?>/<?= $row['gambar_produk']; ?>" class="card-img-top" height="270px" alt="<?= $row['nama_produk']; ?>">
							<div class="card-body">
								<h5 class="card-title"><?= $row['nama_produk']; ?></h5>
								<p class="card-text"><?= rupiah($row['harga_produk']) ?></p>
							</div>
							<?php if ($cek) { ?>
								<?php if ($_SESSION['datauser']['role_id'] == 3) { ?>
									<div class="card-footer">
										<input type="number" id="qty<?= $row['id_produk']; ?>" value="1" class="form-control mb-2">
										<button class="btn btn-block btn-md" onclick="addToCart(this)" data-tes='<?= json_encode($row); ?>' style="background-color: #1f5e86; color:white"><i class=" fa fa-cart-plus"></i></button>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					<?php endforeach; ?>
				<?php } else { ?>
					<div class="card mr-3 mt-4" style="width: 20%; border:0px;">
						<div class="card-body">
							<h5 class="card-title text-danger">Belum Ada Data</h5>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-12 usePadding bg-light" id="kontakKami">
			<center>
				<div class="header mb-5">
					<h2>Kontak Kami</h2>
					<hr style="border-bottom: 5px solid #f0fbff;margin-top: 0px;margin-bottom: 20px;border-top: 0px;">
				</div>
			</center>
			<div class="card bg-light" style="border:0px">
				<table class="table table-borderless">
					<tr>
						<th>PT. XYZ</th>
						<th>Lokasi Kami</th>
						<th>Punya Pertanyaan?</th>
					</tr>
					<tr>
						<td style="max-width: 250px;">Belanja garam dengan murah, mudah dan cepat</td>
						<td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.576920268957!2d112.77756815769997!3d-7.2888818233843695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fa46b89209b3%3A0x15fff2b17bae273f!2sJl.%20Arief%20Rahman%20Hakim%20No.93%2C%20Klampis%20Ngasem%2C%20Kec.%20Sukolilo%2C%20Kota%20SBY%2C%20Jawa%20Timur%2060117!5e0!3m2!1sid!2sid!4v1658389674131!5m2!1sid!2sid" width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></td>
						<td style="max-width: 250px;">
							<p><i class="fa fa-map-marker mr-2"></i> Jalan Arif Rahman Hakim No.93 Surabaya, Jawa Timur, Indonesia 60117</p>
							<p><i class="fa fa fa-phone mr-2"></i> (031) 5937581</p>
							<p><a href="mailto:sekretariat@ptxyz.com" style="color:black"><i class="fa fa-envelope mr-2"></i> sekretariat@ptxyz.com</a></p>
						</td>
					</tr>
				</table>
			</div>
			<div class="row ml-2">
				<a style="color:black;" class="mr-2" href="#">
					<h4><i class="fa fa-facebook-official"></i></h4>
				</a>
				<a style="color:black;" class="mr-2" href="https://twitter.com/ptxyzpersero/">
					<h4><i class="fa fa-twitter-square"></i></h4>
				</a>
				<a style="color:black;" class="mr-2" href="https://www.instagram.com/ptxyzpersero/?hl=id">
					<h4><i class="fa fa-instagram"></i></h4>
				</a>
			</div>
		</div>
	</div>
	<div class="card-footer" style="background-color: #061387; color:white">
		<center>
			Ⓒ 2022 All rights reserved | Made with 💙 for every people.
		</center>
	</div>

	<!-- <img src="https://i.pinimg.com/originals/19/17/12/191712aa83814872d5964169fd08c1c3.png" alt=""> -->

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
			var i;
			var x = document.getElementsByClassName("mySlides");
			for (i = 0; i < x.length; i++) {
				x[i].style.display = "none";
			}
			myIndex++;
			if (myIndex > x.length) {
				myIndex = 1
			}
			x[myIndex - 1].style.display = "block";
			setTimeout(carousel, 2000); // Change image every 2 seconds
		}

		mybutton = document.getElementById("myBtn");

		// When the user scrolls down 20px from the top of the document, show the button
		window.onscroll = function() {
			scrollFunction()
		};

		function scrollFunction() {
			if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				mybutton.style.display = "block";
			} else {
				mybutton.style.display = "none";
			}
		}

		function topFunction() {
			document.body.scrollTop = 0; // For Safari
			document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
		}
	</script>

	<script>
		$(document).ready(function() {

		});

		async function addToCart(obj) {
			var a = await JSON.stringify($(obj).data('tes'));
			if (a) {
				var asem = JSON.parse(a);

				try {
					$.ajax({
						type: 'POST',
						url: `<?= base_url('keranjang/addCart') ?>`,
						data: {
							'data': a,
							'id_user': `<?= $this->session->userdata('datauser')['id_user'] ?>`,
							'qty': $('#qty' + asem['id_produk']).val(),
						},
						async: true,
						dataType: 'json',
						success: function(response) {
							if (response == "success") {
								alert('Berhasil tambah ke keranjang!');
								window.location.reload();
							} else {
								alert('Gagal tambah ke keranjang :(');
							}
						},
						error: function(error) {
							console.log(error);
						}
					});
				} catch (error) {
					console.log(error);
				}
			}

		}
	</script>
</body>

</html>