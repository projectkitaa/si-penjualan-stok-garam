<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='shorcut icon' href="<?= base_url('assets/') ?>img/logo.png">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style type="text/css">
        .usePadding {
            margin: 0px;
            padding-left: 7%;
            padding-right: 7%;
            padding-top: 2%;
            padding-bottom: 3%;
        }

        .usePadding2 {
            margin: 0px;
            padding-left: 7%;
            padding-right: 7%;
        }

        html {
            scroll-behavior: smooth;
        }

        #myBtn {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Fixed/sticky position */
            bottom: 20px;
            /* Place the button at the bottom of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
            border: none;
            /* Remove borders */
            outline: none;
            /* Remove outline */
            background-color: #1f5e86;
            /* Set a background color */
            color: white;
            /* Text color */
            cursor: pointer;
            /* Add a mouse pointer on hover */
            padding: 15px;
            /* Some padding */
            border-radius: 10px;
            /* Rounded corners */
            font-size: 18px;
            /* Increase font size */
        }

        .mySlides {
            display: none;
        }

        #myBtn:hover {
            background-color: #fce600;
        }

        #cartButton:hover {
            background-color: #fce600;
        }
    </style>
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <div class="content">
        <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-fw fa-arrow-up"></i></button>

        <nav class="navbar col-md-12" id="nav1" style="background-color: #061387; color:white;">
            <div class="nav-menu ml-3">
                <div class="row">
                    <span class="nav-item nav-link "><i class="fa fa fa-phone"></i> (081) 5437621</span>
                    <a class="nav-item nav-link" href="mailto:sekretariat@ptxyz.com" style="color:white"><i class="fa fa-envelope"></i> sekretariat@ptxyz.com</a>
                </div>
            </div>
            <div class="nav-menu mr-3">
                <div class="row">
                    <a style="color:white" class="nav-item nav-link" href="#"><i class="fa fa-facebook-official"></i></i></a>
                    <a style="color:white" class="nav-item nav-link" href="https://twitter.com/ptxyzpersero/"><i class="fa fa-twitter-square"></i></a>
                    <a style="color:white" class="nav-item nav-link" href="https://www.instagram.com/ptxyzpersero/?hl=id"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </nav>
        <hr id="hr1" style="border-bottom: 5px solid #fac800;margin-top: 0px;margin-bottom: 2px;border-top: 0px;">

        <nav class="navbar mb-3 usePadding2" style="background-color: white;">
            <img src="<?= base_url('assets/') ?>img/logo.png" width="4%" class="d-inline-block align-top" alt="">
            <div class="nav-menu">
                <div class="row">
                    <a href="<?= base_url('landingPage') ?>" class="nav-link" style="color: black;">
                        Home
                    </a>
                    <a class="nav-item nav-link" href="<?= base_url('landingPage') ?>#produkKami" style="color: black;">
                        Produk
                    </a>
                    <a class="nav-item nav-link" href="<?= base_url('landingPage') ?>#tentangKami" style="color: black;">
                        Tentang Kami
                    </a>
                    <a class="nav-item nav-link" href="<?= base_url('landingPage') ?>#kontakKami" style="color: black;">
                        Kontak
                    </a>
                    <?php if ($cek) { ?>
                        <div class="nav-item dropdown">
                            <a style="color: black;" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Akun
                            </a>
                            <div class="dropdown-menu" tabindex="-1" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?= base_url('pelanggan/dashboard') ?>">Dashboard</a>
                                <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">Logout</a>
                            </div>
                        </div>
                        <a class="nav-item nav-link" href="<?= base_url('keranjang') ?>" style="color: white; background-color: #1f5e86;">
                            <i class="fa fa-shopping-cart"></i> [<?= $jumlahKeranjang; ?>]
                        </a>
                    <?php } else { ?>
                        <div class="nav-item dropdown">
                            <a style="color: black;" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Akun
                            </a>
                            <div class="dropdown-menu" tabindex="-1" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?= base_url('auth') ?>">Login</a>
                                <a class="dropdown-item" href="<?= base_url('auth/register') ?>">Register</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </nav>

        <div class="col-md-12 usePadding" id="produkKami">
            <center>
                <div class="header">
                    <h2>Keranjang Saya</h2>
                    <hr style="border-bottom: 5px solid #fac800;margin-top: 0px;margin-bottom: 20px;border-top: 0px;">
                </div>
            </center>
            <div class="card" style="border:0px">
                <div class="card-body">
                    <div class="row mb-2" style="justify-content: center;">
                        <?php if (count($keranjang) != 0) { ?>
                            <?php foreach ($keranjang as $row) : ?>
                                <div class="card mr-3 mt-4" style="width: 20%;">
                                    <img src="<?= base_url('uploads') ?>/<?= $row['gambar_produk']; ?>" class="card-img-top" height="270px" alt="<?= $row['nama_produk']; ?>">
                                    <div class="card-body">
                                        <h5 class="card-title"><?= $row['nama_produk']; ?></h5>
                                        <p class="card-text"><?= $row['jumlah_beli'] ?> || <?= rupiah($row['jumlah_harga']) ?></p>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-block btn-danger btn-md" id="cartButton" onclick="deleteSatuan(<?= $row['id_detail_transaksi'] ?>)"><i class=" fa fa-trash"></i></button>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php } else { ?>
                            <div class="card mr-3 mt-4" style="width: 20%; border:0px;" style="justify-content: center;">
                                <div class="card-body">
                                    <h5 class="card-title text-danger">Belum Ada Data</h5>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-footer" style="border: 0px; background-color: white;">
                    <?php if (!empty($id_transaksi)) : ?>
                        <button onclick="deleteSemua(<?= $id_transaksi ?>)" class=" btn btn-danger btn-md" style="width: 300px;">
                            Kosongkan
                        </button>
                        <a href="<?= base_url('transaksi/pesananSaya') ?>" class="btn float-right btn-primary btn-md" style="width: 300px;">
                            Buy
                        </a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer" style="background-color: #061387; color:white">
        <center>
            Ⓒ 2022 All rights reserved | Made with 💙 for every people.
        </center>
    </div>

    <!-- <img src="https://i.pinimg.com/originals/19/17/12/191712aa83814872d5964169fd08c1c3.png" alt=""> -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

    <script>
        mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }

        function deleteSatuan(id) {
            $.ajax({
                type: 'get',
                url: `<?= base_url('keranjang/deleteSatuan/') ?>` + id,
                success: function(response) {
                    if (response == "success") {
                        alert('Data Keranjang berhasil didelete');
                        window.location.reload();
                    } else {
                        alert('Data Keranjang gagal didelete :(');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        function deleteSemua(id) {
            $.ajax({
                type: 'get',
                url: `<?= base_url('keranjang/deleteKeranjangSemua/') ?>` + id,
                success: function(response) {
                    if (response == "success") {
                        alert('Data semua keranjang berhasil didelete');
                        window.location.reload();
                    } else {
                        alert('Data semua keranjang gagal didelete :(');
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    </script>
</body>

</html>