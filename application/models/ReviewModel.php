<?php

class ReviewModel extends CI_Model
{
    /**
     * Function getReview digunakan untuk mendapatkan seluruh data Review
     */
    public function getReview()
    {
        $this->db->select('*');
        $this->db->from('review');
        $this->db->join('transaksi', 'review.transaksi_id = transaksi.id_transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Function getReviewByUserID digunakan untuk mendapatkan seluruh data Review berdasarkan user
     */
    public function getReviewByUserID($idUser)
    {
        $this->db->select('*');
        $this->db->from('review');
        $this->db->join('transaksi', 'review.transaksi_id = transaksi.id_transaksi');
        $this->db->where('transaksi.user_id', $idUser);
        $query = $this->db->get();
        return $query->result_array();
    }


    /**
     * Function getReviewByID digunakan untuk mendapatkan seluruh data Review berdsasarkan id inputan
     */
    public function getReviewByID($id)
    {
        $this->db->select('*');
        $this->db->from('review');
        $this->db->join('transaksi', 'review.transaksi_id = transaksi.id_transaksi');
        $this->db->where('id_review', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Function storeReview digunakan untuk menginputkan data Review
     */
    public function storeReview($data)
    {
        if ($this->db->insert('review', $data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function prosesDeleteReview digunakan untuk mengahpus data Review berdasarkan id
     */
    public function prosesDeleteReview($id)
    {
        if ($this->db->delete('review', ['id_review' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    public function updateReview($id, $data)
    {
        $this->db->where('id_review', $id);
        if ($this->db->update('review', $data)) {
            return true;
        } else {
            return false;
        }
    }
}
