<?php

class TemporaryFiles extends CI_Model
{

    /**
     * Function storeProduk digunakan untuk menginputkan data produk
     */

    public function get()
    {
        $this->db->select('*');
        $this->db->from('temporary_files');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function store($data)
    {
        if ($this->db->insert('temporary_files', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function destroy()
    {
        if ($this->db->query('DELETE FROM temporary_files')) {
            return true;
        } else {
            return false;
        }
    }
}
