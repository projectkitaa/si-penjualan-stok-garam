<?php

class MetodePembayaranModel extends CI_Model
{
    /**
     * Function getMetode digunakan untuk mendapatkan seluruh data metode
     */
    public function getMetode()
    {
        $this->db->select('*');
        $this->db->from('metode_pembayaran');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMetodeAktif()
    {
        $this->db->select('*');
        $this->db->from('metode_pembayaran');
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Function getMetodeByID digunakan untuk mendapatkan seluruh data metode berdsasarkan id inputan
     */
    public function getMetodeByID($id)
    {
        return $this->db->get_where('metode_pembayaran', ['id_metode' => $id])->result_array();
    }

    /**
     * Function storeMetode digunakan untuk menginputkan data metode
     */
    public function storeMetode($data)
    {
        if ($this->db->insert('metode_pembayaran', $data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function prosesDeleteMetode digunakan untuk mengahpus data metode berdasarkan id
     */
    public function prosesDeleteMetode($id)
    {
        if ($this->db->delete('metode_pembayaran', ['id_metode' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    public function updateMetode($id, $data)
    {
        $this->db->where('id_metode', $id);
        if ($this->db->update('metode_pembayaran', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function aktifkan($id)
    {
        $this->db->where('id_metode', $id);
        if ($this->db->update('metode_pembayaran', ['status' => 1])) {
            return true;
        } else {
            return false;
        }
    }

    public function nonAktifkan($id)
    {
        $this->db->where('id_metode', $id);
        if ($this->db->update('metode_pembayaran', ['status' => 0])) {
            return true;
        } else {
            return false;
        }
    }
}
