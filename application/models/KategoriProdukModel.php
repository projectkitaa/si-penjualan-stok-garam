<?php

class KategoriProdukModel extends CI_Model
{
    /**
     * Function getKategoriProduk digunakan untuk mendapatkan seluruh data jenis produkl
     */
    public function getKategoriProduk()
    {
        $query = $this->db->get_where('kategori_produk');
        return $query->result_array();
    }

    /**
     * Function getKategoriProdukByID digunakan untuk mendapatkan seluruh data kategori berdsasarkan id inputan
     */
    public function getKategoriProdukByID($id)
    {
        return $this->db->get_where('kategori_produk', ['id_kategori' => $id])->result_array();
    }

    /**
     * Function storeKategori digunakan untuk menginputkan data kategori
     */
    public function storeKategori($data)
    {
        if ($this->db->insert('kategori_produk', $data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function prosesDeleteKategori digunakan untuk mengahpus data metode berdasarkan id
     */
    public function prosesDeleteKategori($id)
    {
        if ($this->db->delete('kategori_produk', ['id_kategori' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    public function updateKategori($id, $data)
    {
        $this->db->where('id_kategori', $id);
        if ($this->db->update('kategori_produk', $data)) {
            return true;
        } else {
            return false;
        }
    }
}
