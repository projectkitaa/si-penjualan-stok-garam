<?php

class ProdukModel extends CI_Model
{
    /**
     * Function getProduk digunakan untuk mendapatkan seluruh data produk
     */
    public function getProduk()
    {
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->join('kategori_produk', 'kategori_produk.id_kategori = produk.kategori_id', 'left');
        $this->db->order_by('produk.kategori_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Function getProdukByID digunakan untuk mendapatkan seluruh data produk berdsasarkan id inputan
     */
    public function getProdukByID($id)
    {
        return $this->db->get_where('produk', ['id_produk' => $id])->result_array();
    }

    /**
     * Function getProdukByKategoriID digunakan untuk mendapatkan seluruh data produk berdsasarkan id kategori inputan
     */
    public function getProdukByKategoriID($id)
    {
        return $this->db->get_where('produk', ['kategori_id' => $id])->result_array();
    }

    /**
     * Function storeProduk digunakan untuk menginputkan data produk
     */
    public function storeProduk($data)
    {
        if ($this->db->insert('produk', $data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function prosesDeleteProduk digunakan untuk mengahpus data produk berdasarkan id
     */
    public function prosesDeleteProduk($id)
    {
        if (file_exists('./uploads/' . $this->getProdukByID($id)[0]['gambar_produk'])) {
            array_map('unlink', glob('./uploads/' . $this->getProdukByID($id)[0]['gambar_produk']));
        }
        if ($this->db->delete('produk', ['id_produk' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    public function updateProduk($id, $data)
    {
        $this->db->where('id_produk', $id);
        if ($this->db->update('produk', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function countStokBarang()
    {
        $this->db->select('sum(jumlah_stok) as jumlahBarang');
        $this->db->from('produk');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getProdukLimit()
    {
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->join('kategori_produk', 'kategori_produk.id_kategori = produk.kategori_id', 'left');
        $this->db->order_by('produk.created_at', 'DESC');
        $this->db->limit(3);
        $query = $this->db->get();
        return $query->result_array();
    }
}
