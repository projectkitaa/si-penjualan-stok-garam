<?php

class UserModel extends CI_Model
{
    /**
     * Function getAdminGudang digunakan untuk mendapatkan seluruh data user admin gudang
     */
    public function getAdminGudang()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Function getPelanggan digunakan untuk mendapatkan seluruh data user pelanggan
     */
    public function getPelanggan()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id', 3);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUserByID($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id_user', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Function updateUser digunakan untuk mengeupdate data user di database
     */
    public function updateUser($data, $id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->update('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function aktifkan digunakan untuk mengaktifkan user di database
     */
    public function aktifkan($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->update('users', ['status' => 1])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function nonAktifkan digunakan untuk nonAktifkan user di database
     */
    public function nonAktifkan($id)
    {
        $this->db->where('id_user', $id);
        if ($this->db->update('users', ['status' => 0])) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserByTransaksiID($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('transaksi', 'transaksi.user_id = users.id_user');
        $this->db->where('transaksi.id_transaksi', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPelangganLimit()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id', 3);
        $this->db->order_by('users.created_at', 'DESC');
        $this->db->limit(4);
        $query = $this->db->get();
        return $query->result_array();
    }
}
