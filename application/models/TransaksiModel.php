<?php

use PhpParser\Node\Expr\FuncCall;

class TransaksiModel extends CI_Model
{

    public function get($id = null)
    {
        if ($id) {
            $this->db->select('*');
            $this->db->from('transaksi');
            $this->db->join('users', 'transaksi.user_id = users.id_user');
            $this->db->where('id_transaksi', $id);
            $this->db->where('status_transaksi !=', 4);
            $query = $this->db->get();
            return $query->result_array();
        } else {

            $this->db->select('*');
            $this->db->from('transaksi');
            $this->db->join('users', 'transaksi.user_id = users.id_user');
            $this->db->where('status_transaksi !=', 4);
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    public function checkCart($id_user = null)
    {
        if ($id_user) {

            return $this->db->get_where('transaksi', ['user_id' => $id_user, 'status_transaksi' => 4])->row();
        }
    }

    public function add($data)
    {
        $post = [
            'user_id' => $data['id_user'],
            'status_transaksi' => 4,
            'created_at' => dateTime(),
        ];
        if ($this->db->insert('transaksi', $post)) {
            $last_id = $this->db->insert_id();
            $kode = 'G-' . date('ymdhis') . str_pad($last_id, 6, '0', STR_PAD_LEFT);
            $kode = (string) $kode;
            $upd = [
                'kode_transaksi' => $kode,
            ];

            $this->addDetail($data, $last_id);
            $this->update($last_id, $upd);
            return true;
        } else {
            return false;
        }
    }


    public function addDetail($data, $last_id)
    {

        $decode = json_decode($data['data']);
        $a = $this->checkDetail($decode->id_produk, $last_id);
        if (!$a) {
            $detailTransaksi = [
                'produk_id' => $decode->id_produk,
                'transaksi_id' => $last_id,
                'jumlah_beli' => $data['qty'],
                'jumlah_berat' => $data['qty'] * $decode->berat_produk,
                'jumlah_harga' => $data['qty'] * $decode->harga_produk,
            ];
            $this->db->insert('detail_transaksi', $detailTransaksi);
        } else {
            $this->db->where('id_detail_transaksi', $a->id_detail_transaksi);
            $detailTransaksi = [
                'produk_id' => $decode->id_produk,
                'transaksi_id' => $last_id,
                'jumlah_beli' => $data['qty'],
                'jumlah_berat' => $data['qty'] * $decode->berat_produk,
                'jumlah_harga' => $data['qty'] * $decode->harga_produk,
            ];

            if ($this->db->update('detail_transaksi', $detailTransaksi)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function checkDetail($id = null, $id_trans = null)
    {
        if ($id && $id_trans) {
            return $this->db->get_where('detail_transaksi', ['transaksi_id' => $id_trans, 'produk_id' => $id])->row();
        }
    }

    public function update($id, $data)
    {
        $this->db->where('id_transaksi', $id);
        if ($this->db->update('transaksi', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getTransaksiByUser($idUser)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('transaksi.user_id', $idUser);
        $this->db->where('transaksi.status_transaksi !=', 4);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function getTransaksiProsesByUser($idUser)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('transaksi.user_id', $idUser);
        $this->db->where('transaksi.status_transaksi', 1);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function getKeranjang($idUser)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('detail_transaksi', 'detail_transaksi.transaksi_id = transaksi.id_transaksi');
        $this->db->join('produk', 'detail_transaksi.produk_id = produk.id_produk');
        $this->db->where('transaksi.user_id', $idUser);
        $this->db->where('transaksi.status_transaksi', 4);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function getKeranjangByTransaksi($id)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('detail_transaksi', 'detail_transaksi.transaksi_id = transaksi.id_transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->join('produk', 'detail_transaksi.produk_id = produk.id_produk');
        $this->db->where('transaksi.id_transaksi', $id);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function getKeranjangByTransaksiByMecel($id)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('detail_transaksi', 'detail_transaksi.transaksi_id = transaksi.id_transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->join('produk', 'detail_transaksi.produk_id = produk.id_produk');
        $this->db->where('transaksi.id_transaksi', $id);
        $query = $this->db->get()->row();

        return $query;
    }

    public function getTransaksiSelesai($idUser = null, $notIn = null)
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        if ($notIn != null) {
            $this->db->where_not_in('id_transaksi', $notIn);
        }
        if ($idUser != null) {
            $this->db->where('user_id', $idUser);
        }
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('status_transaksi', 3);
        $query = $this->db->get()->result_array();

        // dd($this->db->last_query());//kalo mau liat query

        return $query;
    }

    public function getPesanan()
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('transaksi.status_transaksi !=', 4);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function deleteKeranjangSatuan($id)
    {
        if ($this->db->delete('detail_transaksi', ['id_detail_transaksi' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteKeranjangSemua($id)
    {
        if ($this->db->delete('detail_transaksi', ['transaksi_id' => $id])) {
            return true;
        } else {
            return false;
        }
    }

    public function sum($id, $kategori)
    {
        if ($kategori == 'harga') {
            $this->db->select('sum(detail_transaksi.jumlah_harga) as harga');
        } else if ($kategori == 'berat') {
            $this->db->select('sum(detail_transaksi.jumlah_berat) as berat');
        }
        $this->db->from('detail_transaksi');
        $this->db->where('detail_transaksi.transaksi_id', $id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function updatePesanan($id, $data)
    {
        $this->db->where('id_transaksi', $id);
        if ($this->db->update('transaksi', $data)) {
            $keranjang = $this->getKeranjangByTransaksi($id);
            foreach ($keranjang as $row) {
                $produk = $this->db->get_where('produk', ['id_produk' =>  $row['produk_id']])->result_array();
                $data = [
                    'jumlah_stok' => $produk[0]['jumlah_stok'] - $row['jumlah_beli']
                ];
                if ($data['jumlah_stok'] <= 0) {
                    return false;
                } else {
                    $this->db->where('id_produk', $row['produk_id']);
                    $this->db->update('produk', $data);
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public function getTransaksiLimit()
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('transaksi.status_transaksi !=', 4);
        $this->db->limit(3);
        $this->db->order_by('transaksi.created_at', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPendapatanHari()
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->where('status_transaksi', 3);
        $this->db->where('DAY(created_at)', date("d"));
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getPendapatanBulan()
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->where('status_transaksi', 3);
        $this->db->where('MONTH(created_at)', date("m"));
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getPendapatanTahun()
    {
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->where('status_transaksi', 3);
        $this->db->where('YEAR(created_at)', date("Y"));
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getDataPerMonth()
    {
        $i = 1;
        $hasil = array();
        while ($i < 13) {
            $this->db->select('SUM(COALESCE(total_pembayaran,0)) AS total_pembayaran');
            $this->db->from('transaksi');
            $this->db->where('MONTH(created_at)', $i);
            $this->db->where('YEAR(created_at)', date("Y"));
            $this->db->where('status_transaksi', 3);
            $data = $this->db->get()->result_array();
            if ($data[0]['total_pembayaran'] == null) {
                $data[0]['total_pembayaran'] = 0;
            }
            array_push($hasil, $data[0]['total_pembayaran']);
            $i++;
        }

        return $hasil;
    }

    public function getTransaksibyProdukID($id)
    {
        $this->db->select('*');
        $this->db->from('detail_transaksi');
        $this->db->where('produk_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
}
