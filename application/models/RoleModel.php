<?php

class RoleModel extends CI_Model
{
    /**
     * Function getRole digunakan untuk mendapatkan seluruh data role
     */
    public function getRole()
    {
        $query = $this->db->get_where('role');
        return $query->result_array();
    }
}
