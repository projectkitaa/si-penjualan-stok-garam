<?php

class PembayaranModel extends CI_Model
{
    /**
     * Function getPembayaranByMetodeID digunakan untuk mendapatkan seluruh data Pembayaran berdsasarkan id Metode inputan
     */
    public function getPembayaranByMetodeID($id)
    {
        return $this->db->get_where('pembayaran', ['metode_id' => $id])->result_array();
    }

    public function getPembayaran($id = null)
    {
        if ($id) {
            $this->db->select('*');
            $this->db->from('pembayaran');
            $this->db->join('transaksi', 'pembayaran.transaksi_id = transaksi.id_transaksi');
            $this->db->join('users', 'transaksi.user_id = users.id_user');
            $this->db->where('transaksi.user_id', $id);
        } else {
            $this->db->select('*');
            $this->db->from('pembayaran');
            $this->db->join('transaksi', 'pembayaran.transaksi_id = transaksi.id_transaksi');
            $this->db->join('users', 'transaksi.user_id = users.id_user');
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPembayaranByID($id)
    {
        $this->db->select('*');
        $this->db->from('pembayaran');
        $this->db->join('transaksi', 'pembayaran.transaksi_id = transaksi.id_transaksi');
        $this->db->join('metode_pembayaran', 'pembayaran.metode_id = metode_pembayaran.id_metode');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('pembayaran.id_pembayaran', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPembayaranNon()
    {
        $this->db->select('*');
        $this->db->from('pembayaran');
        $this->db->join('transaksi', 'pembayaran.transaksi_id = transaksi.id_transaksi');
        $this->db->join('metode_pembayaran', 'pembayaran.metode_id = metode_pembayaran.id_metode');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        $this->db->where('pembayaran.status_pembayaran', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function store($data)
    {
        if ($this->db->insert('pembayaran', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        $this->db->where('id_pembayaran', $id);
        if ($this->db->update('pembayaran', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getPembayaranLimit()
    {
        $this->db->select('*');
        $this->db->from('pembayaran');
        $this->db->join('transaksi', 'pembayaran.transaksi_id = transaksi.id_transaksi');
        $this->db->join('users', 'transaksi.user_id = users.id_user');
        // $this->db->where('transaksi.status_transaksi !=', 4);
        $this->db->order_by('pembayaran.created_at', 'DESC');
        $this->db->limit(3);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function aktifkan($idPembayaran, $idTransaksi, $plat)
    {
        $this->db->where('id_pembayaran', $idPembayaran);
        if ($this->db->update('pembayaran', ['status_pembayaran' => 1])) {
            $this->db->where('id_transaksi', $idTransaksi);
            if ($this->db->update('transaksi', ['plat_truk' => $plat, 'status_transaksi' => 1])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
