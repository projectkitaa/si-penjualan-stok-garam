<?php

class Transaksi extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Role Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaksiModel', 'tr');
        cekAccess('notFound');
    }

    /**
     * Function index digunakan untuk menuju tampilan pesanan
     */
    public function index()
    {
        $dataUser = $this->session->userdata('datauser');
        if ($dataUser['role_id'] == 3) {
            $pesanan = $this->tr->getTransaksiByUser($dataUser['id_user']);
        } else {
            $pesanan = $this->tr->getPesanan();
        }
        $dataView = [
            'title' => 'Pesanan',
            'pesanan' => $pesanan,
        ];

        $this->template->render('transaksi/pesanan/index', $dataView);
    }

    /**
     * Function index digunakan untuk menuju tampilan pesanan
     */
    public function detailPesanan($id)
    {
        $dataView = [
            'title' => 'Detail Pesanan',
            'transaksi' => $this->tr->get($id),
            'keranjang' => $this->tr->getKeranjangByTransaksi($id),
        ];
        $this->template->render('transaksi/pesanan/detail_pesanan', $dataView);
    }

    /**
     * Function pesananSaya digunakan untuk menuju tampilan pesananSaya
     */
    public function pesananSaya()
    {
        $dataUser = $this->session->userdata('datauser');
        $keranjang = $this->tr->getKeranjang($dataUser['id_user']);
        $cek = $this->tr->checkCart($dataUser['id_user']);

        $dataView = [
            'title' => 'Pesanan Saya',
            'keranjang' => $keranjang,
            'transaksi' => $this->tr->get($cek->id_transaksi ?? null),
            'datauser' => $dataUser,
            'raw' => $cek,
        ];

        $this->template->render('transaksi/pesanan/pesananSaya', $dataView);
    }

    public function storePesanan($id)
    {
        $data = [
            'status_transaksi' => 0,
            'total_pembayaran' => $this->tr->sum($id, 'harga')[0]['harga'],
            'total_berat' => $this->tr->sum($id, 'berat')[0]['berat'],
            'tanggal_transaksi' => dateTime(),
        ];

        if ($this->tr->updatePesanan($id, $data)) {
            $this->session->set_flashdata('message', pesanSukses('Silahkan lakukan pembayaran!'));
        } else {
            $this->session->set_flashdata('message', pesanGagal('Gagal memproses!'));
            redirect('transaksi/pesananSaya');
        }

        redirect("pembayaran/detailPembayaran/$id");
    }

    public function updateStatus($id)
    {
        $data = [
            'status_transaksi' => $this->input->post('status'),
        ];

        if ($this->tr->updatePesanan($id, $data)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
}
