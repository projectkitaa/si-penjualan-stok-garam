<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Role Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('roleModel', 'role');
    }

    /**
     * Function index digunakan untuk menuju tampilan role
     */
    public function index()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Role',
            'dataRole' => $this->role->getRole(),
        ];
        $this->template->render('role/index', $dataView);
    }
}
