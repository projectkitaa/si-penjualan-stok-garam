<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keranjang extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Role Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaksiModel', 'tr');
        cekAccess('pembeli');
    }

    public function index()
    {
        $dataUser = $this->session->userdata('datauser');
        $keranjang = $this->tr->getKeranjang($dataUser['id_user']);
        $cek = $this->tr->checkCart($dataUser['id_user']);
        $dataView = [
            'title' => 'Keranjang',
            'cek' => $dataUser,
            'keranjang' => $keranjang,
            'jumlahKeranjang' => count($keranjang),
            'id_transaksi' => $cek->id_transaksi ?? null,
        ];
        $this->load->view('keranjang/index', $dataView);
    }

    public function addCart()
    {
        $data = $this->input->post();
        $cek = $this->tr->checkCart($data['id_user']);
        if (!$cek) {
            $this->tr->add($data);
            echo json_encode("success");
        } else {
            echo json_encode("success");
            $this->tr->addDetail($data, $cek->id_transaksi);
        }
    }

    public function deleteSatuan($id)
    {
        if ($this->tr->deleteKeranjangSatuan($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function deleteKeranjangSemua($id)
    {
        if ($this->tr->deleteKeranjangSemua($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
}
