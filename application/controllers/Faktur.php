<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Faktur extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Faktur Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('transaksiModel', 'tr');
    }

    /**
     * Function index digunakan untuk menuju tampilan faktur
     */
    public function index()
    {
        cekAccess('gudang');
        $dataView = [
            'title' => 'Faktur',
            'transaksi' => $this->tr->getTransaksiSelesai()
        ];
        $this->template->render('transaksi/faktur/index', $dataView);
    }

    public function fakturGenerate($id)
    {
        $transaksi = $this->tr->getKeranjangByTransaksi($id);
        $dataView = [
            'title' => 'Generate Factur',
            'transaksi' => $transaksi[0],
            'keranjang' => $this->tr->getKeranjangByTransaksi($id),
        ];
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = $transaksi[0]['kode_transaksi'] . dateTime() . ".pdf";
        $this->pdf->load_view('transaksi/faktur/templateFaktur', $dataView);
        // $this->load->view('transaksi/pesanan/templateFaktur');
    }
}
