<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KategoriProduk extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat KategoriProduk Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('kategoriProdukModel', 'kategoriProduk');
        $this->load->model('produkModel', 'produk');
    }

    /**
     * Function index digunakan untuk menuju tampilan kategori produk
     */
    public function index()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Kategori Produk',
            'dataKategori' => $this->kategoriProduk->getKategoriProduk(),
        ];
        $this->template->render('produk/kategoriProduk', $dataView);
    }

    /**
     * Function store digunakan untuk menginputkan kategori
     */
    public function store()
    {
        $this->form_validation->set_rules(
            'nama_kategori',
            'nama_kategori',
            'required|trim|is_unique[kategori_produk.nama_kategori]',
            [
                'is_unique' => 'Kategori Produk sudah terdaftar!'
            ]
        );

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $namaKategori = $this->input->post('nama_kategori');

            $data = [
                'nama_kategori' => $namaKategori,
                'created_at' => dateTime(),
            ];

            if ($this->kategoriProduk->storeKategori($data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    /**
     * Function updateKategori digunakan untuk mengubah data Kategori yang sudah ada
     */
    public function updateKategori($id)
    {
        $kategori = $this->db->get_where('kategori_produk', ['id_kategori' => $id])->row_array();

        if ($this->input->post('nama_kategori') == $kategori['nama_kategori']) {
            $this->form_validation->set_rules('nama_kategori', 'nama_kategori', 'required|trim');
        } else {
            $this->form_validation->set_rules(
                'nama_kategori',
                'nama_kategori',
                'required|trim|is_unique[kategori_produk.nama_kategori]',
                [
                    'is_unique' => 'Kategori Produk sudah terdaftar!'
                ]
            );
        }

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $namaKategori = $this->input->post('nama_kategori');

            $data = [
                'nama_kategori' => $namaKategori,
                'updated_at' => dateTime(),
            ];

            if ($this->kategoriProduk->updateKategori($id, $data)) {
                $this->session->set_flashdata('message', pesanSukses('Kategori berhasil di update!'));
            } else {
                $this->session->set_flashdata('message', pesanGagal('Kategori gagal di update!'));
            }

            redirect('kategoriProduk');
        }
    }

    /**
     * Function delete digunakan menghapus Kategori yang dipilih
     * dan dengan syarat belum ada produk yang terdaftar dengan Kategori tersebut
     */

    public function delete($id)
    {
        $cek = $this->produk->getProdukByKategoriID($id);

        if (count($cek) != 0) {
            echo "cant";
        } else {
            if ($this->kategoriProduk->prosesDeleteKategori($id)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }
}
