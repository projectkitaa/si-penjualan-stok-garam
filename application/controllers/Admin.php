<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Admin Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('produkModel', 'produk');
        $this->load->model('userModel', 'user');
        $this->load->model('transaksiModel', 'tr');
        $this->load->model('PembayaranModel', 'pembayaran');
        $this->load->model('kategoriProdukModel', 'kategoriProduk');
        cekAccess('admin');
    }

    /**
     * Function index digunakan untuk menuju tampilan dashboard
     */
    public function index()
    {
        $dataView = [
            'title' => 'Dashboard',
            'produk' => count($this->produk->getProduk()),
            'user' => count($this->user->getPelanggan()),
            'pesanan' => count($this->tr->get()),
            'transaksiMonth' => $this->tr->getDataPerMonth(),
            'dataKategori' => $this->kategoriProduk->getKategoriProduk(),
            'pendapatan' => array_sum(array_column($this->tr->getTransaksiSelesai(), 'total_pembayaran')),
            'transaksi' => $this->tr->getTransaksiLimit(),
            'pembayaran' => $this->pembayaran->getPembayaranLimit(),
            'pelanggan' => $this->user->getPelangganLimit(),
            'produktiga' => $this->produk->getProdukLimit(),
            'hariIni' => array_sum(array_column($this->tr->getPendapatanHari(), 'total_pembayaran')),
            'bulanIni' => array_sum(array_column($this->tr->getPendapatanBulan(), 'total_pembayaran')),
            'tahunIni' => array_sum(array_column($this->tr->getPendapatanTahun(), 'total_pembayaran')),
        ];
        $this->template->render('admin/index', $dataView);
    }
}
