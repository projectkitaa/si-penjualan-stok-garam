<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MetodePembayaran extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat MetodePembayaran Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('metodePembayaranModel', 'metode');
        $this->load->model('pembayaranModel', 'pembayaran');
    }

    /**
     * Function index digunakan untuk menuju tampilan metode pembayaran
     */
    public function index()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Metode Pembayaran',
            'dataMetode' => $this->metode->getMetode(),
        ];
        $this->template->render('transaksi/metodePembayaran/index', $dataView);
    }

    /**
     * Function store digunakan untuk menginputkan metode
     */
    public function store()
    {
        $this->form_validation->set_rules(
            'nama_metode',
            'nama_metode',
            'required|trim|is_unique[metode_pembayaran.nama_metode]',
            [
                'is_unique' => 'Metode Pembayaran sudah terdaftar!'
            ]
        );

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $rekening = $this->input->post('rekening');
            $namaMetode = $this->input->post('nama_metode');
            $an = $this->input->post('an');

            $data = [
                'rekening' => $rekening,
                'nama_metode' => $namaMetode,
                'an' => $an,
                'status' => 1,
                'created_at' => dateTime(),
            ];

            if ($this->metode->storeMetode($data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    /**
     * Function updateMetode digunakan untuk mengubah data Metode yang sudah ada
     */
    public function updateMetode($id)
    {
        $metode = $this->db->get_where('metode_pembayaran', ['id_metode' => $id])->row_array();

        if ($this->input->post('nama_metode') == $metode['nama_metode']) {
            $this->form_validation->set_rules('nama_metode', 'nama_metode', 'required|trim');
        } else {
            $this->form_validation->set_rules(
                'nama_metode',
                'nama_metode',
                'required|trim|is_unique[metode_pembayaran.nama_metode]',
                [
                    'is_unique' => 'Metode Pembayaran sudah terdaftar!'
                ]
            );
        }

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $rekening = $this->input->post('rekening');
            $namaMetode = $this->input->post('nama_metode');
            $an = $this->input->post('an');

            $data = [
                'rekening' => $rekening,
                'nama_metode' => $namaMetode,
                'an' => $an,
                'status' => 1,
                'updated_at' => dateTime(),
            ];

            if ($this->metode->updateMetode($id, $data)) {
                $this->session->set_flashdata('message', pesanSukses('Metode berhasil di update!'));
            } else {
                $this->session->set_flashdata('message', pesanGagal('Metode gagal di update!'));
            }

            redirect('metodePembayaran');
        }
    }

    /**
     * Function delete digunakan menghapus Metode yang dipilih
     * dan dengan syarat belum ada pembayaran yang terdaftar dengan metode tersebut
     */

    public function delete($id)
    {
        $cek = $this->pembayaran->getPembayaranByMetodeID($id);

        if (count($cek) != 0) {
            echo "cant";
        } else {
            if ($this->metode->prosesDeleteMetode($id)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    /**
     * Function aktifkan digunakan mengaktifkan metode pembayaran
     */

    public function aktifkan($id)
    {
        if ($this->metode->aktifkan($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
    /**
     * Function nonAktifkan digunakan mengnonAktifkan metode pembayaran
     */

    public function nonAktifkan($id)
    {
        if ($this->metode->nonAktifkan($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
}
