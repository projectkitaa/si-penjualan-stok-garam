<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelanggan extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Pelanggan Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('userModel', 'pelanggan');
        $this->load->model('reviewModel', 'review');
        $this->load->model('PembayaranModel', 'pembayaran');
        $this->load->model('transaksiModel', 'tr');
    }

    /**
     * Function dashboard digunakan untuk menuju tampilan dashboard
     */
    public function dashboard()
    {
        cekAccess('pembeli');
        $dataUser = $this->session->userdata('datauser');

        $dataView = [
            'title' => 'Dashboard',
            'order' => count($this->tr->getTransaksiByUser($dataUser['id_user'])),
            'orderProses' => count($this->tr->getTransaksiProsesByUser($dataUser['id_user'])),
            'pembayaran' => count($this->pembayaran->getPembayaran($dataUser['id_user'])),
            'review' => count($this->review->getReviewByUserID($dataUser['id_user'])),
        ];
        $this->template->render('pelanggan/dashboard', $dataView);
    }

    /**
     * Function index digunakan untuk menuju tampilan index
     */
    public function index()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Pelanggan',
            'dataPelanggan' => $this->pelanggan->getPelanggan(),
        ];
        $this->template->render('pelanggan/index', $dataView);
    }

    public function getByid($id)
    {
        $data = $this->pelanggan->getUserByID($id);
        echo json_encode($data);
    }
}
