<?php

class Pembayaran extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Role Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaksiModel', 'tr');
        $this->load->model('PembayaranModel', 'pembayaran');
        $this->load->model('temporaryFiles', 'tmp');
        $this->load->model('metodePembayaranModel', 'metode');
    }

    public function index()
    {
        cekAccess('notFound');

        $pembayaran = $this->pembayaran->getPembayaran();
        $dataView = [
            'title' => 'Pembayaran',
            'dataPembayaran' => $pembayaran,
        ];
        $this->template->render('transaksi/pembayaran/index', $dataView);
    }

    /**
     * Function index digunakan untuk menuju tampilan verifikasi pembayaran
     */
    public function verifikasiPembayaran()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Verifikasi Pembayaran',
            'pembayaran' => $this->pembayaran->getPembayaranNon(),
        ];
        $this->template->render('transaksi/verifikasiPembayaran/index', $dataView);
    }

    public function pembayaran()
    {
        cekAccess('notFound');

        $pembayaran = $this->pembayaran->getPembayaran();

        $dataUser = $this->session->userdata('datauser');
        if ($dataUser['role_id'] == 3) {
            $pembayaran = $this->pembayaran->getPembayaran($dataUser['id_user']);
            $arr = array_column($pembayaran, 'transaksi_id');
            $transaksiSelesai = $this->tr->getTransaksiSelesai($dataUser['id_user'], $arr);
        }

        $dataView = [
            'title' => 'Pembayaran',
            'dataPembayaran' => $pembayaran,
        ];

        if ($dataUser['role_id'] == 3) {
            $dataView['transaksiSelesai'] = $transaksiSelesai;
        }
        $this->template->render('transaksi/pembayaran/index', $dataView);
    }

    public function detailPembayaran($a = null, $b = null)
    {
        cekAccess('notFound');
        $id = $this->input->get('transaksi_id');

        $dataView = [
            'title' => 'Detail Pembayaran',
            'metodePembayaran' => $this->metode->getMetodeAktif(),
            'pembayaran' => $this->pembayaran->getPembayaranByID($b),
            'keranjang' => $this->tr->getKeranjangByTransaksi($a ?? $id),
            'asemrowo' => $this->tr->getKeranjangByTransaksiByMecel($a ?? $id),
        ];
        $this->template->render('transaksi/pembayaran/detail_pembayaran', $dataView);
    }


    public function edit($id, $trid)
    {
        cekAccess('notFound');
        $dataView = [
            'title' => 'Edit Detail Pembayaran',
            'metodePembayaran' => $this->metode->getMetodeAktif(),
            'pembayaran' => $this->pembayaran->getPembayaranByID($id),
            'keranjang' => $this->tr->getKeranjangByTransaksi($trid),
            'asemrowo' => $this->tr->getKeranjangByTransaksiByMecel($trid),
        ];
        $this->template->render('transaksi/pembayaran/edit_detail_pembayaran', $dataView);
    }

    public function process()
    {
        if (isset($_FILES['buktiBayar'])) {
            $config['upload_path']          = './uploads/tmp/';
            $config['allowed_types']        = 'jpeg|jpg|png|pdf';
            $config['max_size']             = 200000;
            $config['max_width']            = 2024;
            $config['max_height']           = 1768;
            // taruh temporary
            // 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload("buktiBayar")) {
                $a = $this->upload->data();
                $data = array(
                    'nama_file' => $a['file_name'],
                );
                $b = $this->tmp->store($data);
            }
        }
    }

    public function revert()
    {
        array_map('unlink', glob('./uploads/tmp/' . '*'));
        // array_map('unlink', glob('./uploads/' . 'WhatsApp_Image_2022-07-17_at_08_01_11.jpeg'));
        $this->tmp->destroy();
    }

    public function store()
    {

        copy('./uploads/tmp/' . $this->tmp->get()[0]['nama_file'], './uploads/' . $this->tmp->get()[0]['nama_file']);
        $data = [
            'transaksi_id' => $this->input->post('transaksi_id'),
            'metode_id' => $this->input->post('metode_id'),
            'bukti_pembayaran' => $this->tmp->get()[0]['nama_file'],
            'status_pembayaran' => 0,
            'created_at' => dateTime(),
            'tanggal_transfer' => dateTime(),
        ];

        if ($this->pembayaran->store($data)) {
            echo "success";
            array_map('unlink', glob('./uploads/tmp/' . '*'));
        } else {
            echo "failed";
        }
    }


    public function update()
    {

        if ($this->tmp->get()[0]['nama_file']) {
            if (file_exists('./uploads/tmp/' . $this->tmp->get()[0]['nama_file'])) {

                copy('./uploads/tmp/' . $this->tmp->get()[0]['nama_file'], './uploads/' . $this->tmp->get()[0]['nama_file']);
                if (file_exists('./uploads/' . $this->input->post('bukti_pembayaran')) && !empty($this->input->post('bukti_pembayaran'))) {

                    array_map('unlink', glob('./uploads/' . $this->input->post('bukti_pembayaran')));
                }

                array_map('unlink', glob('./uploads/tmp/' . '*'));
                $data = [
                    'metode_id' => $this->input->post('metode_id'),
                    'bukti_pembayaran' => $this->tmp->get()[0]['nama_file'],
                    'created_at' => dateTime(),
                    'tanggal_transfer' => dateTime(),
                ];
            } else {
                $data = [
                    'metode_id' => $this->input->post('metode_id'),
                    'created_at' => dateTime(),
                    'tanggal_transfer' => dateTime(),
                ];
            }
        } else {

            $data = [
                'metode_id' => $this->input->post('metode_id'),
                'created_at' => dateTime(),
                'tanggal_transfer' => dateTime(),
            ];
        }


        $this->tmp->destroy();

        if ($this->pembayaran->update($this->input->post('pembayaran_id'), $data)) {
            echo "success";
        } else {
            echo "failed";
        }
    }

    public function aktifkan($idPembayaran, $idTransaksi)
    {
        $plat = $this->input->post('plat_truk');
        if ($this->pembayaran->aktifkan($idPembayaran, $idTransaksi, $plat)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
}
