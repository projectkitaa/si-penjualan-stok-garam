<?php

class Produk extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat Produk Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('produkModel', 'produk');
        $this->load->model('transaksiModel', 'transaksi');
        $this->load->model('kategoriProdukModel', 'kategoriProduk');
        $this->load->model('temporaryFiles', 'tmp');
    }

    /**
     * Function index digunakan untuk menuju tampilan produk
     */
    public function index()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Produk',
            'dataProduk' => $this->produk->getProduk(),
            'kategori' => $this->kategoriProduk->getKategoriProduk()
        ];
        $this->template->render('produk/index', $dataView);
    }

    /**
     * Function store digunakan untuk menginputkan produk
     */
    public function store()
    {
        copy('./uploads/tmp/' . $this->tmp->get()[0]['nama_file'], './uploads/' . $this->tmp->get()[0]['nama_file']);

        $this->form_validation->set_rules(
            'nama_produk',
            'nama_produk',
            'required|trim|is_unique[produk.nama_produk]',
            [
                'is_unique' => 'Produk sudah terdaftar!'
            ]
        );

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $idKategori = $this->input->post('id_kategori');
            $namaProduk = $this->input->post('nama_produk');
            $berat = $this->input->post('berat_produk');
            $hargaProduk = $this->input->post('harga_produk');
            $data = [
                'kategori_id' => $idKategori,
                'nama_produk' => $namaProduk,
                'jumlah_stok' => 0,
                'berat_produk' => $berat,
                'harga_produk' => $hargaProduk,
                'gambar_produk' =>  $this->tmp->get()[0]['nama_file'],
                'created_at' => dateTime(),
            ];

            if ($this->produk->storeProduk($data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
        $this->revert();
    }

    /**
     * Function updateProduk digunakan untuk mengubah data Produk yang sudah ada
     */
    public function updateProduk()
    {

        $id = $this->input->post('id_produk');
        $produk = $this->db->get_where('Produk', ['id_Produk' => $id])->row_array();
        if ($this->input->post('nama_produk')) {
            if ($this->input->post('nama_produk') == $produk['nama_produk']) {
                $this->form_validation->set_rules('Produk', 'Application', 'required|trim');
            } else {
                $this->form_validation->set_rules(
                    'nama_produk',
                    'nama_produk',
                    'required|trim|is_unique[produk.nama_produk]',
                    [
                        'is_unique' => 'Produk sudah terdaftar!'
                    ]
                );
            }
            if ($this->form_validation->run() == false) {
                $this->index();
            }
        }

        $idKategori = $this->input->post('id_kategori');
        $namaProduk = $this->input->post('nama_produk');
        $hargaProduk = $this->input->post('harga_produk');
        $berat = $this->input->post('berat_produk');
        if ($this->tmp->get()[0]['nama_file']) {
            if (file_exists('./uploads/tmp/' . $this->tmp->get()[0]['nama_file'])) {

                copy('./uploads/tmp/' . $this->tmp->get()[0]['nama_file'], './uploads/' . $this->tmp->get()[0]['nama_file']);
                if (file_exists('./uploads/' . $this->input->post('gambar_produk')) && !empty($this->input->post('gambar_produk'))) {
                    array_map('unlink', glob('./uploads/' . $this->input->post('gambar_produk')));
                }
                array_map('unlink', glob('./uploads/tmp/' . '*'));
                $data = [
                    'kategori_id' => $idKategori,
                    'nama_produk' => $namaProduk,
                    'harga_produk' => $hargaProduk,
                    'berat_produk' => $berat,
                    'gambar_produk' => $this->tmp->get()[0]['nama_file'],
                    'updated_at' => dateTime(),
                ];
            } else {
                $data = [
                    'kategori_id' => $idKategori,
                    'nama_produk' => $namaProduk,
                    'harga_produk' => $hargaProduk,
                    'berat_produk' => $berat,
                    'updated_at' => dateTime(),
                ];
            }
        } else {

            $data = [
                'kategori_id' => $idKategori,
                'nama_produk' => $namaProduk,
                'harga_produk' => $hargaProduk,
                'berat_produk' => $berat,
                'updated_at' => dateTime(),
            ];
        }

        $this->tmp->destroy();
        if ($this->produk->updateProduk($id, $data)) {
            $this->session->set_flashdata('message', pesanSukses('Produk berhasil di update!'));
        } else {
            $this->session->set_flashdata('message', pesanGagal('Produk gagal di update!'));
        }

        redirect('produk');
    }

    public function getByid($id)
    {
        $data = $this->produk->getProdukByID($id);
        echo json_encode($data);
    }

    /**
     * Function delete digunakan menghapus Produk yang dipilih
     * dan dengan syarat belum ada transaksi yang terdaftar dengan produk tersebut
     */
    public function delete($id)
    {

        $cek = $this->transaksi->getTransaksibyProdukID($id);
        if (count($cek) != 0) {
            echo "cant";
        } else {

            if ($this->produk->prosesDeleteProduk($id)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    public function process()
    {
        if (isset($_FILES['foto_produk'])) {
            $config['upload_path']          = './uploads/tmp/';
            $config['allowed_types']        = 'jpeg|jpg|png';
            $config['max_size']             = 200000;
            $config['max_width']            = 2024;
            $config['max_height']           = 1768;
            // taruh temporary
            // 
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload("foto_produk")) {
                $a = $this->upload->data();
                $data = array(
                    'nama_file' => $a['file_name'],
                );
                $b = $this->tmp->store($data);
            }
        }
    }

    public function revert()
    {
        array_map('unlink', glob('./uploads/tmp/' . '*'));
        // array_map('unlink', glob('./uploads/' . 'WhatsApp_Image_2022-07-17_at_08_01_11.jpeg'));
        $this->tmp->destroy();
    }

    public function tambahStok()
    {
        $id = $_POST['id'];
        $stok = $_POST['stok'];

        $query = $this->db->select('jumlah_stok')
            ->where('id_produk', $id)
            ->get('produk')->result_array();

        $stokBaru = $stok + $query[0]['jumlah_stok'];

        $data = [
            "jumlah_stok" => $stokBaru
        ];

        $this->db->update('produk', $data, array('id_produk' => $id));

        redirect('produk/kelolaStok');
    }

    public function kelolaStok()
    {
        cekAccess('gudang');
        $dataView = [
            'title' => 'Kelola Stok',
            'dataProduk' => $this->produk->getProduk(),
            'kategori' => $this->kategoriProduk->getKategoriProduk()
        ];
        $this->template->render('produk/stokProduk', $dataView);
    }
}
