<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminGudang extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat AdminGudang Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('userModel', 'adminGudang');
        $this->load->model('authModel', 'auth');
        $this->load->model('produkModel', 'produk');
        $this->load->model('transaksiModel', 'tr');
        $this->load->model('PembayaranModel', 'pembayaran');
    }

    /**
     * Function index digunakan untuk menuju tampilan dashboard
     */
    public function index()
    {
        cekAccess('gudang');
        $dataView = [
            'title' => 'Dashboard',
            'pelanggan' => count($this->adminGudang->getPelanggan()),
            'stokProduk' => array_sum(array_column($this->produk->getProduk(), 'jumlah_stok')),
            'pesanan' => count($this->tr->get()),
            'produk' => count($this->produk->getProduk()),
        ];
        $this->template->render('adminGudang/dashboard', $dataView);
    }

    /**
     * Function store digunakan untuk menambahkan data admin gudang
     */
    public function store()
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_rules('notelp', 'notelp', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $email = $this->input->post('email');
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $notelp = $this->input->post('notelp');
            $alamat = $this->input->post('alamat');
            $nama = $this->input->post('nama');

            $data = [
                'nama' => ucwords($nama),
                'email' => ucfirst($email),
                'notelp' => $notelp,
                'alamat' => $alamat,
                'role_id' => 1,
                'password' => $password,
                'created_at' => dateTime(),
            ];

            if ($this->auth->storeUser($data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    /**
     * Function update digunakan untuk mengubah data Admin Gudang yang sudah ada
     */
    public function update($id)
    {
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_rules('notelp', 'notelp', 'trim|required');

        $user = $this->db->get_where('users', ['id_user' => $id])->row_array();

        if ($this->input->post('email') == $user['email']) {
            $this->form_validation->set_rules('email', 'email', 'required|trim');
        } else {
            $this->form_validation->set_rules(
                'email',
                'email',
                'required|trim|is_unique[user.email]',
                [
                    'is_unique' => 'Email sudah digunakan!'
                ]
            );
        }

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $email = $this->input->post('email');
            $notelp = $this->input->post('notelp');
            $alamat = $this->input->post('alamat');
            $nama = $this->input->post('nama');

            $data = [
                'nama' => ucwords($nama),
                'email' => ucfirst($email),
                'notelp' => $notelp,
                'alamat' => $alamat,
                'role_id' => 1,
                'updated_at' => dateTime(),
            ];

            if ($this->input->post('password')) {
                $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $data['password'] = $password;
            }

            if ($this->adminGudang->updateUser($data, $id)) {
                $this->session->set_flashdata('message', pesanSukses('Admin Gudang berhasil di update!'));
            } else {
                $this->session->set_flashdata('message', pesanGagal('Admin Gudang gagal di update!'));
            }
            redirect('adminGudang/indexx');
        }
    }

    /**
     * Function indexx digunakan untuk menuju tampilan index
     */
    public function indexx()
    {
        cekAccess('admin');
        $dataView = [
            'title' => 'Admin Gudang',
            'dataAdminGudang' => $this->adminGudang->getAdminGudang(),
        ];
        $this->template->render('adminGudang/index', $dataView);
    }

    public function getByid($id)
    {
        $data = $this->adminGudang->getUserByID($id);
        echo json_encode($data);
    }

    /**
     * Function aktifkan digunakan mengaktifkan admin gudang
     */

    public function aktifkan($id)
    {
        if ($this->adminGudang->aktifkan($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
    /**
     * Function nonAktifkan digunakan mengnonAktifkan admin gudang
     */

    public function nonAktifkan($id)
    {
        if ($this->adminGudang->nonAktifkan($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
}
