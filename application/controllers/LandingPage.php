<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LandingPage extends CI_Controller
{
    /**
     * Function index digunakan untuk menuju tampilan login
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('produkModel', 'produk');
        $this->load->model('transaksiModel', 'tr');
    }

    public function index()
    {
        $dataUser = $this->session->userdata('datauser');
        $dataView = [
            'title' => 'Landing Page',
            'cek' => $dataUser,
            'produk' => $this->produk->getProduk(),
            'dashboard' => getUrlDashboard(),
        ];

        if ($dataUser) {
            $keranjang = $this->tr->getKeranjang($dataUser['id_user']);
            $dataView['jumlahKeranjang'] = count($keranjang);
        }

        $this->load->view('welcome', $dataView);
    }
}
