<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Review extends CI_Controller
{
    /**
     * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
     * saat review Controller dipanggil
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('reviewModel', 'review');
        $this->load->model('transaksiModel', 'tr');
        cekAccess('notFound');
    }

    /**
     * Function index digunakan untuk menuju tampilan review
     */
    public function index()
    {

        $review = $this->review->getReview();

        $dataUser = $this->session->userdata('datauser');
        if ($dataUser['role_id'] == 3) {
            $review = $this->review->getReviewByUserID($dataUser['id_user']);
            $arr = array_column($review, 'transaksi_id');
            $transaksiSelesai = $this->tr->getTransaksiSelesai($dataUser['id_user'], $arr);
        }

        $dataView = [
            'title' => 'Review Pelanggan',
            'dataReview' => $review,
        ];

        if ($dataUser['role_id'] == 3) {
            $dataView['transaksiSelesai'] = $transaksiSelesai;
        }
        $this->template->render('review/index', $dataView);
    }

    public function getByid($id)
    {
        $data = $this->review->getReviewByID($id);
        echo json_encode($data);
    }

    /**
     * Function store digunakan untuk menginputkan review
     */
    public function store()
    {
        $this->form_validation->set_rules(
            'id_transaksi',
            'id_transaksi',
            'required|trim|is_unique[review.transaksi_id]',
            [
                'is_unique' => 'Transaksi sudah direview!'
            ]
        );

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $id_transaksi = $this->input->post('id_transaksi');
            $komentar = $this->input->post('komentar');
            $skor = $this->input->post('skor');

            $data = [
                'skor' => $skor,
                'komentar' => $komentar,
                'transaksi_id' => $id_transaksi,
            ];

            if ($this->review->storeReview($data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    /**
     * Function update digunakan untuk mengubah data Review yang sudah ada
     */
    public function update($id)
    {
        $this->form_validation->set_rules('komentar', 'komentar', 'required|trim');
        $this->form_validation->set_rules('skor', 'skor', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->index();
        } else {
            $komentar = $this->input->post('komentar');
            $skor = $this->input->post('skor');

            $data = [
                'skor' => $skor,
                'komentar' => $komentar,
            ];

            if ($this->review->updateReview($id, $data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    /**
     * Function delete digunakan menghapus Review yang dipilih
     * dan dengan syarat belum ada pembayaran yang terdaftar dengan review tersebut
     */

    public function delete($id)
    {
        if ($this->review->prosesDeleteReview($id)) {
            echo "success";
        } else {
            echo "failed";
        }
    }
}
