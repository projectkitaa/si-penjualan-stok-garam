<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	/**
	 * Function __construct() merupakan fungsi yang di eksekusi pertama kali 
	 * saat Auth Controller dipanggil
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('authModel', 'auth');
	}

	/**
	 * Function index digunakan untuk menuju tampilan login
	 */
	public function index()
	{
		cekAccess('none');
		$dataView = [
			'title' => 'Login',
		];
		$this->template->renderAuth('auth/login', $dataView);
	}

	/**
	 * Function register digunakan untuk menuju tampilan register
	 */
	public function register()
	{
		cekAccess('none');
		$dataView = [
			'title' => 'register',
		];

		$this->template->renderAuth('auth/register', $dataView);
	}

	/**
	 * Function authUser digunakan untuk authentication user
	 */
	public function authUser()
	{
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->index();
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$user = $this->auth->login($email);

			if ($user) {
				$data = [
					'nama' => ucwords($user['nama']),
					'email' => ucfirst($user['email']),
					'alamat' => ucfirst($user['alamat']),
					'notelp' => $user['notelp'],
					'role_id' => $user['role_id'],
					'id_user' => $user['id_user'],
					'password' => $user['password']
				];

				$this->session->set_userdata('datauser', $data);

				if (password_verify($password, $user['password'])) {
					if ($user['role_id'] == 1) {
						redirect('adminGudang');
					} else if ($user['role_id'] == 2) {
						redirect('admin');
					} else if ($user['role_id'] == 3) {
						redirect('landingPage');
					}
				} else {
					$this->session->set_flashdata('message', pesanGagal('Password Salah!'));
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata('message', pesanGagal('Akun belum terdaftar!'));
				redirect('auth');
			}
		}
	}

	/**
	 * Function store digunakan untuk menambahkan data pelanggan
	 */
	public function store()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
		$this->form_validation->set_rules('notelp', 'notelp', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->index();
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$notelp = $this->input->post('notelp');
			$alamat = $this->input->post('alamat');
			$nama = $this->input->post('nama');

			$data = [
				'nama' => ucwords($nama),
				'email' => ucfirst($email),
				'notelp' => $notelp,
				'alamat' => $alamat,
				'role_id' => 3,
				'password' => $password,
				'created_at' => dateTime(),
			];

			if ($this->auth->storeUser($data)) {
				$this->session->set_flashdata('message', pesanSukses('Akun telah didaftarkan. Silahkan login!'));
				redirect('landingPage');
			} else {
				$this->session->set_flashdata('message', pesanGagal('Gagal mendaftarkan akun!'));
				redirect('auth/register');
			}
		}
	}

	/**
	 * function logout digunakan untuk logout dan destroy session
	 */
	public function logout()
	{
		$this->session->unset_userdata('datauser');
		$this->session->set_flashdata('message', pesanSukses('Berhasil logout!'));
		$this->index();
	}

	/**
	 * Function blocked digunakan untuk menampilkan halaman blocked access
	 */
	public function blocked()
	{
		$session_data = $this->session->userdata('datauser');
		$dataView = [
			'title' => 'blocked',
			'role' => $session_data['role_id']
		];

		$this->load->view('auth/blocked', $dataView);
	}
}
