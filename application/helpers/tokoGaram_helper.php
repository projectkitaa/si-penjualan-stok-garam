<?php

/**
 * Function ini digunakan untuk menampilkan alert sukses
 */
function pesanSukses($data)
{
    return '<div class="alert alert-success" role="alert">' . $data . '</div>';
}

/**
 * Function ini digunakan untuk menampilkan alert danger
 */
function pesanGagal($data)
{
    return '<div class="alert alert-danger" role="alert">' . $data . '</div>';
}

/**
 * Function dd digunakan untuk var dump data / mengecek data
 */
function dd($data)
{
    echo "<pre>" . var_export($data) . "</pre>";
    die();
}

/**
 * Function cekAccess untuk mengecek apakah user sudah login saat membuka aplikasi
 * dan mengecek apakah user memiliki role sesuai
 */
function cekAccess($role)
{
    $ci = get_instance();
    $session_data = $ci->session->userdata('datauser');

    if ($role == 'gudang') {
        if (!$session_data) {
            $ci->session->set_flashdata('message', pesanGagal('Silahkan Login Terlebih Dahulu!'));
            redirect('auth');
        } else if ($session_data['role_id'] != 1) {
            redirect('auth/blocked');
        }
    } else if ($role == 'admin') {
        if (!$session_data) {
            $ci->session->set_flashdata('message', pesanGagal('Silahkan Login Terlebih Dahulu!'));
            redirect('auth');
        } else if ($session_data['role_id'] != 2) {
            redirect('auth/blocked');
        }
    } else if ($role == 'pembeli') {
        if (!$session_data) {
            $ci->session->set_flashdata('message', pesanGagal('Silahkan Login Terlebih Dahulu!'));
            redirect('auth');
        } else if ($session_data['role_id'] != 3) {
            redirect('auth/blocked');
        }
    } else if ($role == 'none') {
        if ($session_data) {
            $ci->session->set_flashdata('message', pesanGagal('Silahkan Logout Terlebih Dahulu!'));
            if ($session_data['role_id'] == 1) {
                redirect('adminGudang');
            } else if ($session_data['role_id'] == 2) {
                redirect('admin');
            } else if ($session_data['role_id'] == 3) {
                redirect('landingPage');
            }
        }
    } else if ($role == 'notFound') {
        if (!$session_data) {
            $ci->session->set_flashdata('message', pesanGagal('Silahkan Login Terlebih Dahulu!'));
            redirect('auth');
        }
    }
}

function rupiah($jumlah)
{
    if (!is_numeric($jumlah)) {
        $jumlah = 0;
    }
    return "Rp. " . number_format((float) $jumlah, 2, ',', '.');
}

function getUrlDashboard()
{
    $ci = get_instance();
    $session_data = $ci->session->userdata('datauser');
    if ($session_data) {
        if ($session_data['role_id'] == 1) {
            $urlDashboard = 'adminGudang';
        } else if ($session_data['role_id'] == 2) {
            $urlDashboard = 'admin';
        } else if ($session_data['role_id'] == 3) {
            $urlDashboard = 'pelanggan/dashboard';
        }
    } else {
        $urlDashboard = null;
    }

    return $urlDashboard;
}

function dateTime()
{
    return date("Y-m-d H:i:s");
}

function toDate($data = null)
{
    if ($data) {
        return date('d-M-Y H:i:s', strtotime($data));
    } else {
        return '-';
    }
}
